package cn.symbio.system.mapper;

import cn.symbio.system.domain.Menu;
import cn.symbio.basic.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
public interface MenuMapper extends BaseMapper<Menu> {

    void addMiddle(@Param("rid") Long id, @Param("menus") List<Long> menus);

    void deleteMiddle(Long id);

    List<Menu> findByMenu(Long id);
}
