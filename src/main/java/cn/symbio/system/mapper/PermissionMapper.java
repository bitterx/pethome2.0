package cn.symbio.system.mapper;

import cn.symbio.system.domain.Permission;
import cn.symbio.basic.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
public interface PermissionMapper extends BaseMapper<Permission> {

    Permission loadBySn(String permissionSn);

    void addMiddle(@Param("rid") Long id, @Param("permissions") List<Long> permissions);

    void deleteMiddle(Long id);

    List<String> findByPermissions(Long id);
}
