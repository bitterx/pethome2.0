package cn.symbio.system.mapper;

import cn.symbio.system.domain.Role;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
public interface RoleMapper extends BaseMapper<Role> {

}
