package cn.symbio.system.controller;

import cn.symbio.system.service.IMenuService;
import cn.symbio.system.domain.Menu;
import cn.symbio.system.query.MenuQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/menu")
public class MenuController {
    @Autowired
    public IMenuService menuService;


    /**
     * 保存和修改公用的
     * @param menu  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Menu menu){
        try {
            if( menu.getId()!=null)
                menuService.update(menu);
            else
                menuService.add(menu);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            menuService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public Menu get(@PathVariable("id")Long id)
    {
        return menuService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<Menu> list(){

        return menuService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping
    public PageList<Menu> json(@RequestBody MenuQuery query)
    {
        return menuService.queryByPage(query);
    }
    /**
    * 菜单新增上级部门树
    */
    @GetMapping("/first")
    public List<Menu> first()
    {
        return menuService.first();
    }
    /**
    * 角色新增菜单数
    */
    @GetMapping("/tree")
    public List<Menu> tree()
    {
        return menuService.tree();
    }
}
