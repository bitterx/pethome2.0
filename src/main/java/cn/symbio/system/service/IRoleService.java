package cn.symbio.system.service;

import cn.symbio.system.domain.Role;
import cn.symbio.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
public interface IRoleService extends IBaseService<Role> {

}
