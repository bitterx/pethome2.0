package cn.symbio.system.service;

import cn.symbio.basic.util.PageList;
import cn.symbio.system.domain.Menu;
import cn.symbio.basic.service.IBaseService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
public interface IMenuService extends IBaseService<Menu> {

    List<Menu> first();

    List<Menu> tree();
}
