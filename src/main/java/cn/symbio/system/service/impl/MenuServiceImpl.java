package cn.symbio.system.service.impl;

import cn.symbio.org.domain.Department;
import cn.symbio.system.domain.Menu;
import cn.symbio.system.service.IMenuService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
@Service
public class MenuServiceImpl extends BaseServiceImpl<Menu> implements IMenuService {


    // 菜单管理新增下拉上级菜单
    @Override
    public List<Menu> first() {
        List<Menu> menu = super.findAll();
        return menu.stream().filter(menu1 -> null == menu1.getParentId()).collect(Collectors.toList());
    }

    // 角色新增分配菜单权限
    @Override
    public List<Menu> tree() {
        List<Menu> all = super.findAll();
        List<Menu> menuArrayList = new ArrayList<>();
        Map<Long, Menu> map = all.stream().collect(Collectors.toMap(Menu::getId, e -> e));
        all.forEach(menu -> {
            if (null == menu.getParentId()) {
                menuArrayList.add(menu);
            } else {
                Menu menu1 = map.get(menu.getParentId());
                if (null == menu1) {
                    return;
                }
                menu1.getChildren().add(menu);
            }
        });
        return menuArrayList;
    }
}
