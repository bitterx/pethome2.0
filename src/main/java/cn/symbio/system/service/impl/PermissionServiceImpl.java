package cn.symbio.system.service.impl;

import cn.symbio.system.domain.Permission;
import cn.symbio.system.service.IPermissionService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
@Service
public class PermissionServiceImpl extends BaseServiceImpl<Permission> implements IPermissionService {

}
