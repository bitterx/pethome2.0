package cn.symbio.system.service.impl;

import cn.symbio.system.domain.Role;
import cn.symbio.system.mapper.MenuMapper;
import cn.symbio.system.mapper.PermissionMapper;
import cn.symbio.system.service.IRoleService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<Role> implements IRoleService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public void add(Role role) {
        // 添加角色
        super.add(role);
        // 添加按钮权限中间表
        permissionMapper.addMiddle(role.getId(),role.getPermissions());
        // 添加菜单权限
        menuMapper.addMiddle(role.getId(),role.getMenus());
    }

    @Override
    public void update(Role role) {
        super.update(role);
        // 先删除中间表
        permissionMapper.deleteMiddle(role.getId());
        menuMapper.deleteMiddle(role.getId());
        // 在新增中间表
        // 添加按钮权限中间表
        permissionMapper.addMiddle(role.getId(),role.getPermissions());
        // 添加菜单权限
        menuMapper.addMiddle(role.getId(),role.getMenus());
    }

    @Override
    public void delete(Long id) {
        super.delete(id);
        permissionMapper.deleteMiddle(id);
        menuMapper.deleteMiddle(id);
    }
}
