package cn.symbio.system.service;

import cn.symbio.system.domain.Permission;
import cn.symbio.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
public interface IPermissionService extends IBaseService<Permission> {

}
