package cn.symbio.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role extends BaseDomain{


    private String name;
    private String sn;

    private List<Menu> ownMenus;
    // 权限id集合
    private List<Long> permissions;

    //菜单id
    private List<Long> menus;

}
