package cn.symbio.system.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Permission extends BaseDomain{


    private String name;
    private String url;
    private String descs;
    private String sn;
    private Long menuId;

}
