package cn.symbio.log.mapper;

import cn.symbio.basic.mapper.BaseMapper;
import cn.symbio.log.domain.LogSys;

public interface LogMapper extends BaseMapper<LogSys> {
}
