package cn.symbio.log.service.impl;

import cn.symbio.basic.service.impl.BaseServiceImpl;
import cn.symbio.log.domain.LogSys;
import cn.symbio.log.mapper.LogMapper;
import cn.symbio.log.service.ILogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class LogService extends BaseServiceImpl<LogSys> implements ILogService {

    @Autowired
    private LogMapper logMapper;

    @Override
    public void save(LogSys log) {
        log.setUsername("宇宙无敌暴龙战士");
        logMapper.add(log);
    }
}
