package cn.symbio.log.service;

import cn.symbio.basic.service.IBaseService;
import cn.symbio.log.domain.LogSys;

public interface ILogService extends IBaseService<LogSys> {

    void save(LogSys log);
}
