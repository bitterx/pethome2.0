package cn.symbio.log.controller;

import cn.symbio.basic.util.PageList;
import cn.symbio.log.domain.LogSys;
import cn.symbio.log.query.LogQuery;
import cn.symbio.log.service.ILogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/log")
public class LogController {

    @Autowired
    private ILogService logService;


    /**
     * 查询全部
     *
     * @return
     */
    @PostMapping
    public PageList<LogSys> findAll(@RequestBody LogQuery query) {
        return logService.queryByPage(query);
    }


}
