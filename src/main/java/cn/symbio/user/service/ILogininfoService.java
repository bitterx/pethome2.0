package cn.symbio.user.service;

import cn.symbio.user.domain.Logininfo;
import cn.symbio.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-19
 */
public interface ILogininfoService extends IBaseService<Logininfo> {

}
