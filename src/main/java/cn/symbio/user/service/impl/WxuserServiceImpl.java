package cn.symbio.user.service.impl;

import cn.symbio.user.domain.Wxuser;
import cn.symbio.user.mapper.WxuserMapper;
import cn.symbio.user.service.IWxuserService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-22
 */
@Service
public class WxuserServiceImpl extends BaseServiceImpl<Wxuser> implements IWxuserService {

    @Autowired
    private WxuserMapper wxuserMapper;

    @Override
    public Wxuser findWxNameByLoginId(Long id) {
        return wxuserMapper.findWxNameByLoginId(id);
    }
}
