package cn.symbio.user.service.impl;

import cn.symbio.user.domain.Logininfo;
import cn.symbio.user.service.ILogininfoService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-19
 */
@Service
public class LogininfoServiceImpl extends BaseServiceImpl<Logininfo> implements ILogininfoService {

}
