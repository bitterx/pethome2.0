package cn.symbio.user.service.impl;

import cn.symbio.user.domain.User;
import cn.symbio.user.service.IUserService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-19
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService {

}
