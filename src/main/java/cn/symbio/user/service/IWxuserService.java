package cn.symbio.user.service;

import cn.symbio.user.domain.Wxuser;
import cn.symbio.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-22
 */
public interface IWxuserService extends IBaseService<Wxuser> {

    Wxuser findWxNameByLoginId(Long id);



}
