package cn.symbio.user.service;

import cn.symbio.user.domain.User;
import cn.symbio.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-19
 */
public interface IUserService extends IBaseService<User> {

}
