package cn.symbio.user.query;
import cn.symbio.basic.query.BaseQuery;
import lombok.Data;

/**
 *
 * @author bitter
 * @since 2022-10-22
 */
@Data
public class WxuserQuery extends BaseQuery{
}