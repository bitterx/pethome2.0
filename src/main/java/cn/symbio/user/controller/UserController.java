package cn.symbio.user.controller;

import cn.symbio.basic.note.PreAuthorize;
import cn.symbio.user.service.IUserService;
import cn.symbio.user.domain.User;
import cn.symbio.user.query.UserQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@PreAuthorize()
public class UserController {
    @Autowired
    public IUserService userService;


    /**
     * 保存和修改公用的
     * @param user  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody User user){
        try {
            if( user.getId()!=null)
                userService.update(user);
            else
                userService.add(user);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().fail();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    @PreAuthorize(name = "删除一条用户数据",sn = "user:del")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            userService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().fail();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    @PreAuthorize(name = "id获取一条用户数据",sn = "user:getById")
    public User get(@PathVariable("id")Long id)
    {
        return userService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    @PreAuthorize(name = "获取全部用户数据",sn = "user:getAll")
    public List<User> list(){
        return userService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    @PreAuthorize(name = "用户分页查询",sn = "user:queryPage")
    public PageList<User> json(@RequestBody UserQuery query)
    {
        return userService.queryByPage(query);
    }
}
