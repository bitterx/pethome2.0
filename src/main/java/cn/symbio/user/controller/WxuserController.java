package cn.symbio.user.controller;

import cn.symbio.user.service.IWxuserService;
import cn.symbio.user.domain.Wxuser;
import cn.symbio.user.query.WxuserQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/wxuser")
public class WxuserController {
    @Autowired
    public IWxuserService wxuserService;


    /**
     * 保存和修改公用的
     * @param wxuser  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Wxuser wxuser){
        try {
            if( wxuser.getId()!=null)
                wxuserService.update(wxuser);
            else
                wxuserService.add(wxuser);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            wxuserService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public Wxuser get(@PathVariable("id")Long id)
    {
        return wxuserService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<Wxuser> list(){

        return wxuserService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<Wxuser> json(@RequestBody WxuserQuery query)
    {
        return wxuserService.queryByPage(query);
    }
}
