package cn.symbio.user.mapper;

import cn.symbio.user.domain.Wxuser;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-22
 */
public interface WxuserMapper extends BaseMapper<Wxuser> {

    Wxuser findByOpenId(String openid);

    Wxuser findWxNameByLoginId(Long id);
}
