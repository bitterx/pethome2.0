package cn.symbio.user.mapper;

import cn.symbio.user.domain.User;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-19
 */
public interface UserMapper extends BaseMapper<User> {

    User findByPhone(String phone);

    User findByEmail(String email);

    User findByLogin(Long id);
}
