package cn.symbio.user.mapper;

import cn.symbio.user.domain.Logininfo;
import cn.symbio.basic.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-19
 */
public interface LogininfoMapper extends BaseMapper<Logininfo> {



    Logininfo findByUsername(@Param("username") String username,@Param("type") Integer type);

    Logininfo findByUserIdWx(Long userId);
}
