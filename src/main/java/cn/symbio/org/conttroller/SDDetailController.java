package cn.symbio.org.conttroller;

import cn.symbio.basic.note.SysLog;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.domain.SystemDictionaryDetail;
import cn.symbio.org.query.EmployeeQuery;
import cn.symbio.org.query.SDDetailQuery;
import cn.symbio.org.service.IEmployeeService;
import cn.symbio.org.service.ISDDetailService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/detail")
public class SDDetailController {


    @Autowired
    private ISDDetailService detailService;

    /*
        新增and修改
        @RequestBody:将json转成我们的对象
     */
    @PostMapping("/save")
    @ApiOperation(value = "数据字典明细新增and修改")
    @SysLog("新增或修改一条字典详细数据")
    public AjaxResult addOrUpdate(@RequestBody SystemDictionaryDetail detail) {
        System.out.println(detail);
        if (null == detail.getId()) {
            detailService.add(detail);
        } else {
            detailService.update(detail);
        }
        return AjaxResult.me();
    }

    /*
        删除一条数据
        @PathVariable:接收路径变量
     */
    @DeleteMapping("/{id}")
    @SysLog("删除一条字典详细数据")
    public AjaxResult delete(@PathVariable("id") Long id) {
        detailService.delete(id);
        return AjaxResult.me();
    }
    /*
     * 查询全部
     */
    @GetMapping
    public List<SystemDictionaryDetail> findAll() {
        return detailService.findAll();
    }
    /*
     * 通过id获取一条数据
     */
    @GetMapping("/{id}")
    public SystemDictionaryDetail findBuId(@PathVariable Long id) {
        return detailService.findById(id);
    }


    /*
     * 分页查询
     */
    @PostMapping
    public PageList<SystemDictionaryDetail> queryByPage(@RequestBody SDDetailQuery sdDetailQuery) {
        return detailService.queryByPage(sdDetailQuery);
    }

    @PatchMapping
    public AjaxResult patchDel(@RequestBody List<Long> ids) {
        detailService.patchDel(ids);
        return AjaxResult.me();
    }

}
