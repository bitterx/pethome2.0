package cn.symbio.org.conttroller;

import cn.symbio.basic.note.SysLog;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.org.domain.Department;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.query.DepartmentQuery;
import cn.symbio.org.service.IDepartmentService;
import cn.symbio.org.service.IEmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/dept")
@Api(value = "部门接口api")
public class DepartmentController {


    @Autowired
    private IDepartmentService departmentService;


    @Autowired
    private IEmployeeService employeeService;
    /*
        新增and修改
        @RequestBody:将json转成我们的对象
     */
    @PutMapping("/save")
    @ApiOperation(value = "部门新增and修改")
    @SysLog("新增或修改一条部门数据")
    public AjaxResult addOrUpdate(@RequestBody Department department) {
        System.out.println(department);
        if (null == department.getId()) {
            departmentService.add(department);
        } else {
            departmentService.update(department);
        }
        return AjaxResult.me();
    }

    /*
        删除一条数据
        @PathVariable:接收路径变量
     */
    @DeleteMapping("/{id}")
    @SysLog("删除一条部门数据")
    public AjaxResult delete(@PathVariable("id") Long id) {
        departmentService.delete(id);
        return AjaxResult.me();
    }
    /*
     * 查询全部
     */
    @GetMapping
    public List<Department> findAll() {
        return departmentService.findAll();
    }
    /*
     * 通过id获取一条数据
     */
    @GetMapping("/{id}")
    public Department findBuId(@PathVariable Long id) {
        return departmentService.findById(id);
    }


    /*
    * 分页查询
    */
    @PostMapping
    public PageList<Department> queryByPage(@RequestBody DepartmentQuery departmentQuery) {
        return departmentService.queryByPage(departmentQuery);
    }

    /*
     * 查询抽屉数据
     */
    @GetMapping("/emp/{id}")
    public List<Employee> findEmpByDeptId(@PathVariable("id") Long id) {
        // System.out.println("部门id："+id);
        return employeeService.findEmpByDeptId(id);
    }


    /*
        批量删除
     */
    @PatchMapping()
    @SysLog("批量删除部门数据")
    public AjaxResult patchDel(@RequestBody List<Long> ids) {
        departmentService.patchDel(ids);
        return AjaxResult.me();
    }

    /*
        部门树
     */
    @GetMapping("/tree")
    public List<Department> deptTree() {
        List<Department> departments = departmentService.deptTreeTwo();
        return departments;
    }


}
