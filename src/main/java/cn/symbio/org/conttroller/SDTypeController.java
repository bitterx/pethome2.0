package cn.symbio.org.conttroller;

import cn.symbio.basic.note.SysLog;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.org.domain.SystemDictionaryDetail;
import cn.symbio.org.domain.SystemDictionaryType;
import cn.symbio.org.query.SDDetailQuery;
import cn.symbio.org.query.SDTypeQuery;
import cn.symbio.org.service.ISDDetailService;
import cn.symbio.org.service.ISDTypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/stype")
public class SDTypeController {


    @Autowired
    private ISDTypeService typeService;

    /*
        新增and修改
        @RequestBody:将json转成我们的对象
     */
    @PostMapping("/save")
    @ApiOperation(value = "数据字典类型新增and修改")
    @SysLog("新增或修改一条字典类型数据")
    public AjaxResult addOrUpdate(@RequestBody SystemDictionaryType type) {
        if (null == type.getId()) {
            typeService.add(type);
        } else {
            typeService.update(type);
        }
        return AjaxResult.me();
    }

    /*
        删除一条数据
        @PathVariable:接收路径变量
     */
    @DeleteMapping("/{id}")
    @SysLog("删除一条字典类型数据")
    public AjaxResult delete(@PathVariable("id") Long id) {
        typeService.delete(id);
        return AjaxResult.me();
    }
    /*
        级联删除
        @PathVariable:接收路径变量
     */
    @DeleteMapping("/cascade/{id}")
    @SysLog("级联删除一条字典类型数据")
    public AjaxResult deleteTypeAndDetail(@PathVariable("id") Long id) {
        System.out.println(id);
        typeService.deleteTypeAndDetail(id);
        return AjaxResult.me();
    }
    /*
     * 查询全部
     */
    @GetMapping
    public List<SystemDictionaryType> findAll() {
        return typeService.findAll();
    }
    /*
     * 抽屉查询明细数据
     */
    @GetMapping("/detail/{id}")
    public List<SystemDictionaryDetail> findDetailByDeptId(@PathVariable Long id) {
        List<SystemDictionaryDetail> detailByDeptId = typeService.findDetailByDeptId(id);
        return detailByDeptId;
    }
    /*
     * 通过id获取一条数据
     */
    @GetMapping("/{id}")
    public SystemDictionaryType findBuId(@PathVariable Long id) {
        return typeService.findById(id);
    }


    /*
     * 分页查询
     */
    @PostMapping
    public PageList<SystemDictionaryType> queryByPage(@RequestBody SDTypeQuery sdTypeQuery) {
        return typeService.queryByPage(sdTypeQuery);
    }

    @PatchMapping
    public AjaxResult patchDel(@RequestBody List<Long> ids) {
        typeService.patchDel(ids);
        return AjaxResult.me();
    }

}
