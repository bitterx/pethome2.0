package cn.symbio.org.conttroller;

import cn.symbio.basic.note.SysLog;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.query.EmployeeQuery;
import cn.symbio.org.service.IEmployeeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/emp")
public class EmployeeController {


    @Autowired
    private IEmployeeService employeeService;

    /*
        新增and修改
        @RequestBody:将json转成我们的对象
     */
    @PostMapping("/save")
    @ApiOperation(value = "员工新增and修改")
    @SysLog("新增或修改一条员工数据")
    public AjaxResult addOrUpdate(@RequestBody Employee employee) {
        if (null == employee.getId()) {
            employeeService.add(employee);
        } else {
            employeeService.update(employee);
        }
        return AjaxResult.me();
    }

    /*
        删除一条数据
        @PathVariable:接收路径变量
     */
    @DeleteMapping("/{id}")
    @SysLog("删除一条员工数据")
    public AjaxResult delete(@PathVariable("id") Long id) {
        employeeService.delete(id);
        return AjaxResult.me();
    }
    /*
     * 查询全部
     */
    @GetMapping
    public List<Employee> findAll() {
        return employeeService.findAll();
    }
    /*
     * 通过id获取一条数据
     */
    @GetMapping("/{id}")
    public Employee findBuId(@PathVariable Long id) {
        return employeeService.findById(id);
    }


    /*
     * 分页查询
     */
    @PostMapping
    public PageList<Employee> queryByPage(@RequestBody EmployeeQuery employeeQuery) {
        return employeeService.queryByPage(employeeQuery);
    }

    @PatchMapping
    @SysLog("批量删除员工数据")
    public AjaxResult patchDel(@RequestBody List<Long> ids) {
        employeeService.patchDel(ids);
        return AjaxResult.me();
    }


    @GetMapping("/SMMsg/{shopId}")
    public List<Employee> SMMsg(@PathVariable("shopId") Long shopId) {
        return employeeService.SMMsg(shopId);
    }

}
