package cn.symbio.org.controller;

import cn.symbio.basic.note.PreAuthorize;
import cn.symbio.basic.util.ExcelUtil;
import cn.symbio.org.domain.ShopAuditLog;
import cn.symbio.org.service.IShopService;
import cn.symbio.org.domain.Shop;
import cn.symbio.org.query.ShopQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/shop")
@PreAuthorize()
public class ShopController {
    @Autowired
    public IShopService shopService;


    /**
     * 保存和修改公用的
     * @param shop  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Shop shop){
        System.out.println(shop);
        try {
            if( shop.getId()!=null)
                shopService.update(shop);
            else
                shopService.add(shop);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().fail();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    @PreAuthorize(name = "id店铺删除",sn = "shop:del")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            shopService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().fail();
        }
    }
	
    //获取用户
    @GetMapping("/{shopId}")
    @PreAuthorize(name = "店铺id查询数据",sn = "shop:findById")
    public Shop get(@PathVariable("shopId")Long shopId)
    {
        return shopService.findById(shopId);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    @PreAuthorize(name = "店铺查询所以数据",sn = "shop:findAll")
    public List<Shop> list(){
        return shopService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    @PreAuthorize(name = "店铺分页查询",sn = "shop:queryPage")
    public PageList<Shop> json(@RequestBody ShopQuery query)
    {
        return shopService.queryByPage(query);
    }


    /**
    * 店铺入住
    */
    @PostMapping("/settlement")
    @PreAuthorize(name = "店铺入住",sn = "shop:settlement")
    public AjaxResult settlement(@RequestBody Shop shop) {
        System.out.println(shop);
        if (null == shop.getId()) {
            shopService.settlement(shop);
        } else {
            // 修改状态
            shop.setState(0);
            // 提交到这直接更新
            shopService.update(shop);
        }
        return AjaxResult.me();
    }
    /**
    * 审核通过
    */
    @PostMapping("/audit/pass")
    public AjaxResult pass(@RequestBody ShopAuditLog auditLog)
    {
        shopService.pass(auditLog);
        return AjaxResult.me();
    }
    /**
    * 审核驳回
    */
    @PostMapping("/audit/reject")
    public AjaxResult reject(@RequestBody ShopAuditLog auditLog)
    {
        shopService.reject(auditLog);
        return AjaxResult.me();
    }
    /**
    * 店铺激活
    */
    @GetMapping("/uptState/{shopId}")
    public AjaxResult uptStats(@PathVariable("shopId") Long shopId)
    {
        shopService.uptStats(shopId);
        return AjaxResult.me();
    }
    /**
    * 报表数据
    */
    @GetMapping("/echarts")
    public Map<String,Object> echarts() {
        return shopService.echarts();
    }
    /**
    * 导出
    */
    @GetMapping("/export/excel")
    public void exportExcel(HttpServletResponse response) {
        List<Shop> all = shopService.findAll();
        ExcelUtil.exportExcel(all,null,"店铺信息",Shop.class,"店铺信息.xlsx",response);
    }
    /**
    * 导入
    */
    @PostMapping("/import/excel")
    public void importExcel(@RequestPart("file") MultipartFile file) {
        // 获取到excel表中所有数据Shops
        List<Shop> shops = ExcelUtil.importExcel(file, 0, 1, Shop.class);
        // 批量删除到数据库
        shopService.batchAdd(shops);
    }



}
