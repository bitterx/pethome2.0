package cn.symbio.org.controller;

import cn.symbio.org.service.IShopAuditLogService;
import cn.symbio.org.domain.ShopAuditLog;
import cn.symbio.org.query.ShopAuditLogQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/shopAuditLog")
public class ShopAuditLogController {
    @Autowired
    public IShopAuditLogService shopAuditLogService;


    /**
     * 保存和修改公用的
     * @param shopAuditLog  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody ShopAuditLog shopAuditLog){
        try {
            if( shopAuditLog.getId()!=null)
                shopAuditLogService.update(shopAuditLog);
            else
                shopAuditLogService.add(shopAuditLog);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().fail();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            shopAuditLogService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().fail();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public ShopAuditLog get(@PathVariable("id")Long id)
    {
        return shopAuditLogService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<ShopAuditLog> list(){

        return shopAuditLogService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<ShopAuditLog> json(@RequestBody ShopAuditLogQuery query)
    {
        return shopAuditLogService.queryByPage(query);
    }

    /**
     * 店铺下拉数据
     */
    @GetMapping("/auditLog/{id}")
    public List<ShopAuditLog> auditLog(@PathVariable("id") Long id) {
        List<ShopAuditLog> shopAuditLogs = shopAuditLogService.auditLog(id);
        System.out.println(shopAuditLogs);
        return shopAuditLogs;
    }
}
