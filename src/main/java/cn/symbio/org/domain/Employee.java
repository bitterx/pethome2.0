package cn.symbio.org.domain;

import cn.symbio.basic.domain.BaseDomain;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Employee extends BaseDomain {

    private Long id;
    private String username;
    private String phone;
    private String email;
    private String salt;
    private String password;
    private String comfirmPassword;
    private Integer age;
    private Integer state = 1;
    private Long departmentId;
    private Long logininfoId;
    private Long shopId;
    // 上级部门
    private Department department;

}
