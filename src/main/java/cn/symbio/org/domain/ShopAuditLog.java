package cn.symbio.org.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-10-16
 */
@Data
public class ShopAuditLog extends BaseDomain{

    private Long shopId;
    private Integer auditId;
    private Date auditTime = new Date();
    private String note;
    // 已审核_1","驳回_-1
    private Integer state = -1;

}
