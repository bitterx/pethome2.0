package cn.symbio.org.domain;

import cn.symbio.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class SystemDictionaryType extends BaseDomain {

    private Long id;
    private String sn;
    private String name;

}
