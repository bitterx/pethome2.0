package cn.symbio.org.domain;

import cn.symbio.basic.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department extends BaseDomain {


    // 部门编号
    private String sn;
    // 部门名称
    private String name;
    // 部门的上级分类层级id
    private String dirPath;
    // 部门状态
    private Integer state;
    // 部门管理员
    private Long manager_id;
    // 上级部门
    private Long parent_id;

    // 部门经理
    private Employee manager;

    // 上级部门
    private Department parent;

    // 无限极树
    @JsonInclude(JsonInclude.Include.NON_EMPTY)// 如果集合是空会过滤掉
    private List<Department> children = new ArrayList<>();
}
