package cn.symbio.org.domain;

import cn.symbio.basic.domain.BaseDomain;
import lombok.Data;

@Data
public class SystemDictionaryDetail extends BaseDomain {

    private Long id;
    private String name;
    private Long types_id;

    // 所属类型
    private SystemDictionaryType typename;
}
