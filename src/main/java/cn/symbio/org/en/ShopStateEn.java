package cn.symbio.org.en;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum ShopStateEn {

    WAIT_AUDIT(0,"待审核"),
    HAVE_AUDIT(1,"已审核"),
    REJECT(-1,"驳回"),
    PASS(2,"已激活");

    private int code;
    private String name;


    public static String getNameByCode(Long code){
        //                             将ShopStateEn.values()循环出来
        ShopStateEn shopStateEn = Arrays.stream(ShopStateEn.values())
                /**
                 *  过 滤  for (ShopStateEn stateEn : ShopStateEn.values())
                 *  state就是stateEn
                 *  state.getCode() 获取枚举中的code跟传进来的code比较
                 */
                .filter(state -> state.getCode() == code)
                // 返回一个描述流中某个元素的Optional对象，如果流为空，则返回一个空的Optional对象。
                .findAny()
                // 存在值就返回，没有就返回null
                .orElse(null);
        // 判断是否有值
        return null == shopStateEn?"": shopStateEn.getName();
    }

}
