package cn.symbio.org.query;
import cn.symbio.basic.query.BaseQuery;
import lombok.Data;

/**
 *
 * @author bitter
 * @since 2022-10-16
 */
@Data
public class ShopAuditLogQuery extends BaseQuery{
}