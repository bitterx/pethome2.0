package cn.symbio.org.mapper;

import cn.symbio.org.domain.Shop;
import cn.symbio.basic.mapper.BaseMapper;
import cn.symbio.org.vo.EchartsVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-15
 */
public interface ShopMapper extends BaseMapper<Shop> {

    Shop findByName(String name);

    List<EchartsVo> echarts();

    void batchAdd(List<Shop> shops);
}
