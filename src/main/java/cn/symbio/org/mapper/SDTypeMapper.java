package cn.symbio.org.mapper;

import cn.symbio.basic.mapper.BaseMapper;
import cn.symbio.org.domain.SystemDictionaryDetail;
import cn.symbio.org.domain.SystemDictionaryType;

import java.util.List;

public interface SDTypeMapper extends BaseMapper<SystemDictionaryType> {

    List<SystemDictionaryDetail> findEmpByDeptId(Long id);
}
