package cn.symbio.org.mapper;

import cn.symbio.basic.mapper.BaseMapper;
import cn.symbio.org.domain.SystemDictionaryDetail;

import java.util.List;

public interface SDDetailMapper extends BaseMapper<SystemDictionaryDetail> {

    List<SystemDictionaryDetail> findEmpByDeptId(Long id);

    // 查询抽屉
    List<SystemDictionaryDetail> findDetailByDeptId(Long id);

    // 类型id查询
    List<SystemDictionaryDetail> findByTypeId(Long id);
}
