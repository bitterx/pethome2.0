package cn.symbio.org.mapper;

import cn.symbio.basic.mapper.BaseMapper;
import cn.symbio.org.domain.Department;
import cn.symbio.org.query.DepartmentQuery;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface DepartmentMapper extends BaseMapper<Department> {


}
