package cn.symbio.org.mapper;

import cn.symbio.basic.mapper.BaseMapper;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.query.EmployeeQuery;

import java.util.List;

public interface EmployeeMapper extends BaseMapper<Employee> {


    // 抽屉查询数据
    List<Employee> findEmpByDeptId(Long id);

    Employee findByName(String username);


    Employee findByLoginId(Long id);

    List<Employee> findByShopId(Long shopId);
}
