package cn.symbio.org.mapper;

import cn.symbio.org.domain.ShopAuditLog;
import cn.symbio.basic.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-16
 */
public interface ShopAuditLogMapper extends BaseMapper<ShopAuditLog> {

    List<ShopAuditLog> getAuditLogByShopId(Long id);

    List<ShopAuditLog> getAuditLogByBatchShopId(List<Long> collect);
}
