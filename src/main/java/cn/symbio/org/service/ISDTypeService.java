package cn.symbio.org.service;

import cn.symbio.basic.service.IBaseService;
import cn.symbio.org.domain.SystemDictionaryDetail;
import cn.symbio.org.domain.SystemDictionaryType;

import java.util.List;

public interface ISDTypeService extends IBaseService<SystemDictionaryType> {

    // 级联删除
    void deleteTypeAndDetail(Long id);


    // 抽屉查询数据
    List<SystemDictionaryDetail> findDetailByDeptId(Long id);
}
