package cn.symbio.org.service;

import cn.symbio.basic.service.IBaseService;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.domain.SystemDictionaryDetail;
import cn.symbio.org.domain.SystemDictionaryType;

import java.util.List;

public interface ISDDetailService extends IBaseService<SystemDictionaryDetail> {

    // 抽屉查询数据
    List<SystemDictionaryDetail> findEmpByDeptId(Long id);
}
