package cn.symbio.org.service;

import cn.symbio.org.domain.Shop;
import cn.symbio.basic.service.IBaseService;
import cn.symbio.org.domain.ShopAuditLog;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-15
 */
public interface IShopService extends IBaseService<Shop> {

    void settlement(Shop shop);

    void pass(ShopAuditLog auditLog);

    // 修改状态
    void uptStats(Long id);

    Map<String, Object> echarts();

    void reject(ShopAuditLog auditLog);

    void batchAdd(List<Shop> shops);
}
