package cn.symbio.org.service.impl;

import cn.symbio.basic.service.impl.BaseServiceImpl;
import cn.symbio.org.domain.SystemDictionaryDetail;
import cn.symbio.org.domain.SystemDictionaryType;
import cn.symbio.org.mapper.SDDetailMapper;
import cn.symbio.org.mapper.SDTypeMapper;
import cn.symbio.org.service.ISDDetailService;
import cn.symbio.org.service.ISDTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.ls.LSInput;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class SDTypeServiceImpl extends BaseServiceImpl<SystemDictionaryType> implements ISDTypeService {

    @Autowired
    private SDTypeMapper sdTypeMapper;

    @Autowired
    private SDDetailMapper sdDetailMapper;

    /**
     * 级联删除
     * @param id
     */
    @Override
    @Transactional
    public void deleteTypeAndDetail(Long id) {
        // 先把详细查出来
        List<SystemDictionaryDetail> all = sdDetailMapper.findByTypeId(id);
        // 不为空我才批量删除详细
        if (!all.isEmpty()) {
            List<Long> ids = new ArrayList<>();
            for (SystemDictionaryDetail detail : all) {
                ids.add(detail.getId());
            }
            // 批量删除明细
            sdDetailMapper.patchDel(ids);
            System.out.println("------------------");
        }
        // 删除类型
        sdTypeMapper.delete(id);

    }

    @Override
    public List<SystemDictionaryDetail> findDetailByDeptId(Long id) {
        return sdDetailMapper.findDetailByDeptId(id);
    }
}
