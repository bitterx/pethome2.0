package cn.symbio.org.service.impl;

import cn.symbio.basic.service.impl.BaseServiceImpl;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.domain.SystemDictionaryDetail;
import cn.symbio.org.mapper.EmployeeMapper;
import cn.symbio.org.mapper.SDDetailMapper;
import cn.symbio.org.service.IEmployeeService;
import cn.symbio.org.service.ISDDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class SDDetailServiceImpl extends BaseServiceImpl<SystemDictionaryDetail> implements ISDDetailService {

    @Autowired
    private SDDetailMapper sdDetailMapper;


    @Override
    public List<SystemDictionaryDetail> findEmpByDeptId(Long id) {
        return sdDetailMapper.findEmpByDeptId(id);
    }
}
