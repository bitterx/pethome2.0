package cn.symbio.org.service.impl;

import cn.symbio.org.domain.ShopAuditLog;
import cn.symbio.org.mapper.ShopAuditLogMapper;
import cn.symbio.org.service.IShopAuditLogService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-16
 */
@Service
public class ShopAuditLogServiceImpl extends BaseServiceImpl<ShopAuditLog> implements IShopAuditLogService {

    @Autowired
    private ShopAuditLogMapper shopAuditLogMapper;

    @Override
    public List<ShopAuditLog> auditLog(Long id) {
        return shopAuditLogMapper.getAuditLogByShopId(id);
    }
}
