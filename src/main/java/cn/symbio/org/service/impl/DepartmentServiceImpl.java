package cn.symbio.org.service.impl;

import cn.symbio.basic.service.impl.BaseServiceImpl;
import cn.symbio.basic.util.PageList;
import cn.symbio.org.domain.Department;
import cn.symbio.org.mapper.DepartmentMapper;
import cn.symbio.org.query.DepartmentQuery;
import cn.symbio.org.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.naming.spi.ResolveResult;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true,propagation = Propagation.SUPPORTS)
public class DepartmentServiceImpl extends BaseServiceImpl<Department> implements IDepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;


    @Override
    public void patchDel(List<Long> ids) {
        departmentMapper.patchDel(ids);
    }

    /**
     * 使用双循环实现
     * @return
     */
    @Override
    public List<Department> deptTree() {
        // 查询出来的全部部门
        List<Department> all = departmentMapper.findAll();
        // 声明一个空集合返回给前端
        List<Department> result = new ArrayList<>();
        // 先遍历全部把父亲找出来
        for (Department dept : all) {
            // if通过getParentId是否为null判断是不是父亲
            if (null == dept.getParent_id()) {
                // 为空说明没有上级，是父亲直接添加到集合
                result.add(dept);
            } else {
                // 上级部门不为空，说明有上级
                // 到此时dept能确定都是下级部门
                // 通过在次循环全部部门去找爹
                for (Department parent : all) {
                    // 通过下级部门dept的getParent_id去找爹
                    if (parent.getId().equals(dept.getParent_id())) {
                        //找到了就添加到他爹装儿子的集合中
                        List<Department> children = parent.getChildren();
                        children.add(dept);
                        // 结束
                        break;
                    }
                }
            }
        }
        return result;
    }


    /**
     * 使用map实现
     * @return
     */
    @Override
    public List<Department> deptTreeTwo() {
        // 获取全部部门
        List<Department> all = departmentMapper.findAll();
        // 声明空数组返回前端
        List<Department> result = new ArrayList<>();
        // 存map，k是id，v是dept对象
        Map<Long,Department> map = all.stream().collect(Collectors.toMap(Department::getId,e ->e));
        // 循环遍历数据
        for (Department dept : all) {
            // 通过上级部门是否为空判断是不是父亲
            if (null == dept.getParent_id()) {
                // 是直接添加
                result.add(dept);
            } else {
                // 不是加到父亲的儿子集合中
                // 因为之前已将将数据存到map中
                // 现在通过儿子的上级部门id去map中找到父亲存进去
                Department department = map.get(dept.getParent_id());
                // 获取到父亲的儿子集合
                List<Department> children = department.getChildren();
                // 添加
                children.add(dept);
            }
        }
        return result;
    }



}
