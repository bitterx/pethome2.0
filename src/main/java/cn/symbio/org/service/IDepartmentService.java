package cn.symbio.org.service;

import cn.symbio.basic.service.IBaseService;
import cn.symbio.basic.util.PageList;
import cn.symbio.org.domain.Department;
import cn.symbio.org.query.DepartmentQuery;

import java.util.List;

public interface IDepartmentService extends IBaseService<Department> {



    // 部门无极树
    List<Department> deptTree();
    List<Department> deptTreeTwo();
}
