package cn.symbio.org.service;

import cn.symbio.org.domain.ShopAuditLog;
import cn.symbio.basic.service.IBaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-16
 */
public interface IShopAuditLogService extends IBaseService<ShopAuditLog> {

    List<ShopAuditLog> auditLog(Long id);
}
