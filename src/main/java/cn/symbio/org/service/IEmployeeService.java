package cn.symbio.org.service;

import cn.symbio.basic.service.IBaseService;
import cn.symbio.org.domain.Employee;

import java.util.List;

public interface IEmployeeService extends IBaseService<Employee> {

    // 抽屉查询数据
    List<Employee> findEmpByDeptId(Long id);

    Employee findByLoginId(Long id);

    List<Employee> SMMsg(Long shopId);

}
