package cn.symbio.order.mapper;

import cn.symbio.order.domain.OrderPetAcquisition;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
public interface OrderPetAcquisitionMapper extends BaseMapper<OrderPetAcquisition> {

}
