package cn.symbio.order.mapper;

import cn.symbio.order.domain.OrderProduct;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
public interface OrderProductMapper extends BaseMapper<OrderProduct> {

}
