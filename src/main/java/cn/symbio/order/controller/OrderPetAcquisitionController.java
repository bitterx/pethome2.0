package cn.symbio.order.controller;

import cn.symbio.order.dto.OrderDto;
import cn.symbio.order.service.IOrderPetAcquisitionService;
import cn.symbio.order.domain.OrderPetAcquisition;
import cn.symbio.order.query.OrderPetAcquisitionQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orderPetAcquisition")
public class OrderPetAcquisitionController {
    @Autowired
    public IOrderPetAcquisitionService orderPetAcquisitionService;


    /**
     * 保存和修改公用的
     * @param orderPetAcquisition  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody OrderPetAcquisition orderPetAcquisition){
        try {
            if( orderPetAcquisition.getId()!=null)
                orderPetAcquisitionService.update(orderPetAcquisition);
            else
                orderPetAcquisitionService.add(orderPetAcquisition);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            orderPetAcquisitionService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public OrderPetAcquisition get(@PathVariable("id")Long id)
    {
        return orderPetAcquisitionService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<OrderPetAcquisition> list(){
        return orderPetAcquisitionService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<OrderPetAcquisition> json(@RequestBody OrderPetAcquisitionQuery query)
    {
        return orderPetAcquisitionService.queryByPage(query);
    }


    /**
    * 取消订单
    */
    @GetMapping("/noConfirm/{id}")
    public AjaxResult noConfirm(@PathVariable("id") Long id)
    {
        orderPetAcquisitionService.noConfirm(id);
        return AjaxResult.me();
    }
    /**
    * 确认订单
    */
    @PostMapping("/confirm")
    public AjaxResult confirm(@RequestBody OrderDto orderDto)
    {
        orderPetAcquisitionService.confirm(orderDto);
        return AjaxResult.me();
    }

}
