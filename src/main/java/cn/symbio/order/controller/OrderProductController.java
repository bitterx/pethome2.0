package cn.symbio.order.controller;

import cn.symbio.order.dto.OrderProductDto;
import cn.symbio.order.service.IOrderProductService;
import cn.symbio.order.domain.OrderProduct;
import cn.symbio.order.query.OrderProductQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orderProduct")
public class OrderProductController {
    @Autowired
    public IOrderProductService orderProductService;


    /**
     * 保存和修改公用的
     * @param orderProduct  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody OrderProduct orderProduct){
        try {
            if( orderProduct.getId()!=null)
                orderProductService.update(orderProduct);
            else
                orderProductService.add(orderProduct);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            orderProductService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public OrderProduct get(@PathVariable("id")Long id)
    {
        return orderProductService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<OrderProduct> list(){

        return orderProductService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<OrderProduct> json(@RequestBody OrderProductQuery query)
    {
        return orderProductService.queryByPage(query);
    }
    /**
    *订单确认
    */
    @PostMapping("/confirm")
    public AjaxResult confirm(@RequestBody OrderProductDto dto)
    {
        orderProductService.confirm(dto);
        return AjaxResult.me();
    }

    /**
    *订单取消
    */
    @GetMapping("/noConfirm/{id}")
    public AjaxResult noConfirm(@PathVariable("id") Long id)
    {
        orderProductService.noConfirm(id);
        return AjaxResult.me();
    }
}
