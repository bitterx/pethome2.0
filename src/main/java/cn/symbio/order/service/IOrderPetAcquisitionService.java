package cn.symbio.order.service;

import cn.symbio.order.domain.OrderPetAcquisition;
import cn.symbio.basic.service.IBaseService;
import cn.symbio.order.dto.OrderDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
public interface IOrderPetAcquisitionService extends IBaseService<OrderPetAcquisition> {

    void noConfirm(Long id);

    void confirm(OrderDto orderDto);
}
