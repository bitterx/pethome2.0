package cn.symbio.order.service.impl;

import cn.symbio.basic.config.BussinessException;
import cn.symbio.order.domain.OrderPetAcquisition;
import cn.symbio.order.dto.OrderDto;
import cn.symbio.order.mapper.OrderPetAcquisitionMapper;
import cn.symbio.order.service.IOrderPetAcquisitionService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import cn.symbio.pet.domain.Pet;
import cn.symbio.pet.domain.SearchMasterMsg;
import cn.symbio.pet.domain.SearchMasterMsgAuditLog;
import cn.symbio.pet.mapper.PetMapper;
import cn.symbio.pet.mapper.SearchMasterMsgAuditLogMapper;
import cn.symbio.pet.mapper.SearchMasterMsgMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
@Service
public class OrderPetAcquisitionServiceImpl extends BaseServiceImpl<OrderPetAcquisition> implements IOrderPetAcquisitionService {

    @Autowired
    private OrderPetAcquisitionMapper orderPetAcquisitionMapper;

    @Autowired
    private SearchMasterMsgMapper searchMasterMsgMapper;

    @Autowired
    private SearchMasterMsgAuditLogMapper searchMasterMsgAuditLogMapper;

    @Autowired
    private PetMapper petMapper;

    /**
     * 订单取消
     * @param id
     */
    @Override
    @Transactional
    public void noConfirm(Long id) {
        // 参数校验
        OrderPetAcquisition orderPetAcquisition = orderPetAcquisitionMapper.findById(id);
        if (orderPetAcquisition == null) {
            throw new BussinessException("当前订单不存在！！！");
        }
        // 修改订单状态为-1取消
        orderPetAcquisition.setState(-1);
        orderPetAcquisitionMapper.update(orderPetAcquisition);
        // 修改寻主消息状态
        SearchMasterMsg searchMasterMsg = searchMasterMsgMapper.findById(orderPetAcquisition.getPetId());
        if(searchMasterMsg == null) {
            throw new BussinessException("当前寻主消息不存在！！！");
        }
        searchMasterMsg.setState(1);
        searchMasterMsg.setShopId(null);
        searchMasterMsgMapper.update(searchMasterMsg);

        // 生成订单取消日志
        addLog(searchMasterMsg,"订单已取消！！！");
    }

    /**
     * 订单确认
     * @param orderDto
     */
    @Override
    public void confirm(OrderDto orderDto) {
        // 修改订单
        OrderPetAcquisition orderPetAcquisition = orderPetAcquisitionMapper.findById(orderDto.getId());
        if (orderPetAcquisition == null) {
            throw new BussinessException("当前订单不存在！！！");
        }
        // 修改寻主信息
        SearchMasterMsg searchMasterMsg = searchMasterMsgMapper.findById(orderPetAcquisition.getPetId());
        if (searchMasterMsg == null) {
            throw new BussinessException("当前寻主消息不存在！！！");
        }
        searchMasterMsg.setState(3);
        searchMasterMsg.setPrice(orderDto.getMoney().doubleValue());
        searchMasterMsgMapper.update(searchMasterMsg);

        // 先保存宠物
        Pet pet = initPet(searchMasterMsg);
        petMapper.add(pet);

        // 修改订单状态为1
        orderPetAcquisition.setState(1);
        // 绑定宠物id
        orderPetAcquisition.setPetId(pet.getId());
        // 修改最终价格
        orderPetAcquisition.setPrice(orderDto.getMoney());
        // 订单确认了就修改时间
        orderPetAcquisition.setLastConfirmTime(new Date());

        // 修改订单
        orderPetAcquisitionMapper.update(orderPetAcquisition);

        // 添加成功日志
        addLog(searchMasterMsg,"确认订单成功！！！");
    }


    /**
     * 初始化pet
     * @param searchMasterMsg
     * @return
     */
    private Pet initPet(SearchMasterMsg searchMasterMsg) {
        return Pet
                .builder()
                .name(searchMasterMsg.getName())
                .costprice(searchMasterMsg.getPrice().toString())
                .state(0)
                .typeId(1L)
                .createtime(new Date())
                .shopId(searchMasterMsg.getShopId())
                .build();

    }

    /**
     *  失败保存日志
     * @param searchMasterMsg
     */
    private void addLog(SearchMasterMsg searchMasterMsg,String note) {
        SearchMasterMsgAuditLog smma = new SearchMasterMsgAuditLog();
        smma.setMsgId(searchMasterMsg.getId());
        smma.setNote(note);
        searchMasterMsgAuditLogMapper.add(smma);
    }
}
