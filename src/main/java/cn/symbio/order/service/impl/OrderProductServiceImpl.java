package cn.symbio.order.service.impl;

import cn.symbio.basic.config.BussinessException;
import cn.symbio.order.domain.OrderProduct;
import cn.symbio.order.dto.OrderProductDto;
import cn.symbio.order.mapper.OrderProductMapper;
import cn.symbio.order.service.IOrderProductService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import cn.symbio.service.domain.ProductOnlineAuditLog;
import cn.symbio.service.domain.ProductUntreated;
import cn.symbio.service.mapper.ProductOnlineAuditLogMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Service
public class OrderProductServiceImpl extends BaseServiceImpl<OrderProduct> implements IOrderProductService {

    @Autowired
    private OrderProductMapper orderProductMapper;

    @Autowired
    private ProductOnlineAuditLogMapper productOnlineAuditLogMapper;
    /**
     * 订单确认
     * @param dto
     */
    @Override
    public void confirm(OrderProductDto dto) {
        // 数据校验
        OrderProduct orderProduct = orderProductMapper.findById(dto.getId());
        if (orderProduct == null) {
            throw new BussinessException("当前订单不存在");
        }
        // 修改状态时间钱
        orderProduct.setState(1);
        orderProduct.setPrice(dto.getMoney());
        orderProductMapper.update(orderProduct);
        // 添加日志
        addLog(orderProduct.getProductId(),1,"已将订单确认！！！");
    }

    /**
     * 订单取消
     * @param id
     */
    @Override
    public void noConfirm(Long id) {
        // 数据校验
        OrderProduct orderProduct = orderProductMapper.findById(id);
        if (orderProduct == null) {
            throw new BussinessException("当前订单不存在");
        }
        // 修改订单状态
        orderProduct.setState(-1);
        orderProductMapper.update(orderProduct);
        // 日志
        addLog(orderProduct.getProductId(),-1,"已将订单取消！！！");
    }

    private void addLog(Long id, Integer state, String note) {
        ProductOnlineAuditLog poal = new ProductOnlineAuditLog();
        poal.setProductId(id);
        poal.setState(state);
        poal.setAuditTime(new Date());
        poal.setNote(note);
        productOnlineAuditLogMapper.add(poal);
    }
}
