package cn.symbio.order.service;

import cn.symbio.basic.util.PageList;
import cn.symbio.order.domain.OrderProduct;
import cn.symbio.basic.service.IBaseService;
import cn.symbio.order.dto.OrderProductDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
public interface IOrderProductService extends IBaseService<OrderProduct> {

    void confirm(OrderProductDto dto);

    void noConfirm(Long id);

}
