package cn.symbio.order.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderProduct extends BaseDomain{


    /**
     * 摘要
     */
    private String digest;
    /**
     * 订单状态：0待支付，2待消费，3待确认，1完成，-1取消
     */
    private Integer state = 0;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 订单唯一编号/标识
     */
    private String orderSn;
    private String paySn;
    /**
     * 最后支付时间
     */
    private Date lastPayTime;
    /**
     * 最后确认时间
     */
    private Date lastConfirmTime;
    /**
     * 哪个服务/产品
     */
    private Long productId;
    /**
     * 哪个用户买的服务
     */
    private Long userId;

    // 微信用户名
    private String nickname;
}
