package cn.symbio.order.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderPetAcquisition extends BaseDomain{


    /**
     * 订单唯一编号/标识
     */
    private String orderSn;
    /**
     * 摘要
     */
    private String digest;
    /**
     * 最后确认时间
     */
    private Date lastConfirmTime;
    /**
     * 订单状态：0待支付，1已完成，-1取消
     */
    private Integer state = 0;
    /**
     * 价格
     */
    private BigDecimal price;
    /**
     * 支付类型：0垫付，1余额，2银行转账
     */
    private Integer payType = 0;
    /**
     * 支付单唯一编号/标识
     */
    private String paySn;
    /**
     * 收购哪个宠物
     */
    private Long petId;
    /**
     * 哪个用户的宠物被收购
     */
    private Long userId;
    /**
     * 哪个店铺收购的
     */
    private Long shopId;
    /**
     * 收购宠物的地址
     */
    private String address;
    /**
     * 哪个店铺的店员收购的
     */
    private Long empId;

}
