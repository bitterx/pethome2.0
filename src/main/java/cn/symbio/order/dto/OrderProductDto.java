package cn.symbio.order.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * 接收订单确认参数
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderProductDto {

    // 订单id
    private Long id;
    // 支付类型
    private String payType;
    // 钱
    private BigDecimal money;
}
