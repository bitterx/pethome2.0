package cn.symbio;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@MapperScan("cn.symbio.*.mapper")
// 扫描监听
@ServletComponentScan("cn.symbio.basic.listener")
public class PethomeApp {
    // main方法启动类
    public static void main(String[] args) {
        SpringApplication.run(PethomeApp.class,args);
    }
}
