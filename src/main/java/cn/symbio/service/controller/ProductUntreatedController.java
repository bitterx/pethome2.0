package cn.symbio.service.controller;

import cn.symbio.basic.util.LoginContext;
import cn.symbio.service.domain.Product;
import cn.symbio.service.dto.AcceptDto;
import cn.symbio.service.service.IProductUntreatedService;
import cn.symbio.service.domain.ProductUntreated;
import cn.symbio.service.query.ProductUntreatedQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.user.domain.Logininfo;
import cn.symbio.user.domain.Wxuser;
import cn.symbio.user.service.IWxuserService;
import cn.symbio.user.service.impl.WxuserServiceImpl;
import com.sun.mail.imap.protocol.ID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/productUntreated")
public class ProductUntreatedController {
    @Autowired
    public IProductUntreatedService productUntreatedService;


    @Autowired
    private IWxuserService iWxuserService;

    /**
     * 保存和修改公用的
     * @param productUntreated  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody ProductUntreated productUntreated){
        try {
            if( productUntreated.getId()!=null)
                productUntreatedService.update(productUntreated);
            else
                productUntreatedService.add(productUntreated);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            productUntreatedService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public ProductUntreated get(@PathVariable("id")Long id)
    {
        return productUntreatedService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<ProductUntreated> list(){

        return productUntreatedService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<ProductUntreated> json(@RequestBody ProductUntreatedQuery query)
    {
        return productUntreatedService.queryByPage(query);
    }


    /**
    *  下单
    */
    @PostMapping("/addProduct")
    public AjaxResult addProduct(@RequestBody Product product, HttpServletRequest request)
    {
        Logininfo loginUser = LoginContext.getLoginUser(request);
        Wxuser wxuser = iWxuserService.findWxNameByLoginId(loginUser.getId());
        product.setUserId(wxuser.getId());
        product.setUsername(wxuser.getNickname());
        return productUntreatedService.addProduct(product);
    }

    /**
     *
    *  接单
    */
    @PostMapping("/accept")
    public AjaxResult accept(@RequestBody AcceptDto dto)
    {
        return productUntreatedService.accept(dto);
    }
    /**
    *  拒单
    *
    */
    @PostMapping("/reject/{id}")
    public AjaxResult reject(@PathVariable("id") Long id)
    {
        return productUntreatedService.reject(id);
    }
}
