package cn.symbio.service.controller;

import cn.symbio.service.service.IProductOnlineAuditLogService;
import cn.symbio.service.domain.ProductOnlineAuditLog;
import cn.symbio.service.query.ProductOnlineAuditLogQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/productOnlineAuditLog")
public class ProductOnlineAuditLogController {
    @Autowired
    public IProductOnlineAuditLogService productOnlineAuditLogService;


    /**
     * 保存和修改公用的
     * @param productOnlineAuditLog  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody ProductOnlineAuditLog productOnlineAuditLog){
        try {
            if( productOnlineAuditLog.getId()!=null)
                productOnlineAuditLogService.update(productOnlineAuditLog);
            else
                productOnlineAuditLogService.add(productOnlineAuditLog);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            productOnlineAuditLogService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public ProductOnlineAuditLog get(@PathVariable("id")Long id)
    {
        return productOnlineAuditLogService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<ProductOnlineAuditLog> list(){

        return productOnlineAuditLogService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<ProductOnlineAuditLog> json(@RequestBody ProductOnlineAuditLogQuery query)
    {
        return productOnlineAuditLogService.queryByPage(query);
    }
}
