package cn.symbio.service.controller;

import cn.symbio.basic.util.LoginContext;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.service.IEmployeeService;
import cn.symbio.service.domain.ProductDetail;
import cn.symbio.service.service.IProductDetailService;
import cn.symbio.service.service.IProductService;
import cn.symbio.service.domain.Product;
import cn.symbio.service.query.ProductQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.user.domain.Logininfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    public IProductService productService;

    @Autowired
    public IProductDetailService productDetailService;

    @Autowired
    private IEmployeeService employeeService;

    /**
     * 保存和修改公用的
     * @param product  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Product product, HttpServletRequest request){
        Logininfo logininfo = LoginContext.getLoginUser(request);
        // 通过logininfo_id查询管理员
        Employee employee = employeeService.findByLoginId(logininfo.getId());
        // 设置店铺id
        product.setShopId(employee.getShopId());
        try {
            if( product.getId()!=null)
                productService.update(product);
            else
                productService.add(product);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            productService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public Product get(@PathVariable("id")Long id)
    {
        return productService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<Product> list(){

        return productService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<Product> json(@RequestBody ProductQuery query)
    {
        return productService.queryByPage(query);
    }


    /**
     * 上架
     *
    */
    @PostMapping("/onsale")
    public AjaxResult onsale(@RequestBody List<Long> ids)
    {
        productService.onsale(ids);
        return AjaxResult.me();
    }
    /**
     * 下架
     *
    */
    @PostMapping("/offsale")
    public AjaxResult offsale(@RequestBody List<Long> ids)
    {
        productService.offsale(ids);
        return AjaxResult.me();
    }

    /**
     * 获取详情
     * @param id
     * @return
     */
    @GetMapping("/detail/{id}")
    public ProductDetail goDetail(@PathVariable("id") Long id)
    {
        return productDetailService.findById(id);
    }


    /**
     * 获取服务及详细
     * @param id
     * @return
     */
    @GetMapping("/productAndDetail/{id}")
    public Product productAndDetail(@PathVariable("id") Long id)
    {
        return productService.productAndDetail(id);
    }


}
