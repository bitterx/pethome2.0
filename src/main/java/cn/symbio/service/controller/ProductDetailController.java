package cn.symbio.service.controller;

import cn.symbio.service.service.IProductDetailService;
import cn.symbio.service.domain.ProductDetail;
import cn.symbio.service.query.ProductDetailQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/productDetail")
public class ProductDetailController {
    @Autowired
    public IProductDetailService productDetailService;


    /**
     * 保存和修改公用的
     * @param productDetail  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody ProductDetail productDetail){
        try {
            if( productDetail.getId()!=null)
                productDetailService.update(productDetail);
            else
                productDetailService.add(productDetail);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            productDetailService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public ProductDetail get(@PathVariable("id")Long id)
    {
        return productDetailService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<ProductDetail> list(){

        return productDetailService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<ProductDetail> json(@RequestBody ProductDetailQuery query)
    {
        return productDetailService.queryByPage(query);
    }
}
