package cn.symbio.service.service;

import cn.symbio.service.domain.ProductDetail;
import cn.symbio.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
public interface IProductDetailService extends IBaseService<ProductDetail> {


}
