package cn.symbio.service.service;

import cn.symbio.basic.util.AjaxResult;
import cn.symbio.service.domain.Product;
import cn.symbio.service.domain.ProductUntreated;
import cn.symbio.basic.service.IBaseService;
import cn.symbio.service.dto.AcceptDto;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
public interface IProductUntreatedService extends IBaseService<ProductUntreated> {

    AjaxResult accept(AcceptDto dto);

    AjaxResult reject(Long id);

    AjaxResult addProduct(Product product);

}
