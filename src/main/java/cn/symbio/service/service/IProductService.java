package cn.symbio.service.service;

import cn.symbio.service.domain.Product;
import cn.symbio.basic.service.IBaseService;
import cn.symbio.service.domain.ProductDetail;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
public interface IProductService extends IBaseService<Product> {

    void onsale(List<Long> ids);

    void offsale(List<Long> ids);

    Product productAndDetail(Long id);
}
