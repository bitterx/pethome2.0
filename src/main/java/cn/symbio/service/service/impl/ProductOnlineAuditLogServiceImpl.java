package cn.symbio.service.service.impl;

import cn.symbio.service.domain.ProductOnlineAuditLog;
import cn.symbio.service.service.IProductOnlineAuditLogService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Service
public class ProductOnlineAuditLogServiceImpl extends BaseServiceImpl<ProductOnlineAuditLog> implements IProductOnlineAuditLogService {

}
