package cn.symbio.service.service.impl;

import cn.symbio.service.domain.ProductDetail;
import cn.symbio.service.service.IProductDetailService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Service
public class ProductDetailServiceImpl extends BaseServiceImpl<ProductDetail> implements IProductDetailService {

}
