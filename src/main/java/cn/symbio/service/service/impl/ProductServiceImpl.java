package cn.symbio.service.service.impl;

import cn.symbio.basic.config.BussinessException;
import cn.symbio.basic.util.BaiduAuditUtils;
import cn.symbio.basic.util.FreemarkUtil;
import cn.symbio.pet.domain.Pet;
import cn.symbio.pet.domain.PetOnlineAuditLog;
import cn.symbio.service.domain.Product;
import cn.symbio.service.domain.ProductDetail;
import cn.symbio.service.domain.ProductOnlineAuditLog;
import cn.symbio.service.mapper.ProductDetailMapper;
import cn.symbio.service.mapper.ProductMapper;
import cn.symbio.service.mapper.ProductOnlineAuditLogMapper;
import cn.symbio.service.service.IProductService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements IProductService {


    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductDetailMapper productDetailMapper;

    @Autowired
    private ProductOnlineAuditLogMapper productOnlineAuditLogMapper;

    /**
     * 同时添加详情表
     * @param product
     */
    @Override
    public void add(Product product) {
        product.setSalecount(0L);
        super.add(product);
        // 保存详情表
        ProductDetail detail = product.getDetail();
        detail.setProductId(product.getId());
        productDetailMapper.add(detail);
    }

    /**
     * 上架
     * @param ids
     */
    @Override
    public void onsale(List<Long> ids) {
        Date date = new Date();
        StringBuilder sb = new StringBuilder();
        ProductOnlineAuditLog productOnlineAuditLog = new ProductOnlineAuditLog();
        // stream通过id获取数据查询
        Map<Long, Product> petMap = productMapper.findAll().stream()
                .filter(pet -> pet.getState() == 0)
                .collect(Collectors.toMap(Product::getId, pet -> pet));
        for (Long id : ids) {
            Product product = petMap.get(id);
            if (!BaiduAuditUtils.TextCensor(product.getName())){
                sb.append("服务上架百度AI审核名称失败，服务名称为：" + product.getName() + ";");
            }
            if (product.getResources().split(",").length > 1){
                if (!BaiduAuditUtils.ImgCensor(product.getResources().split(",")[0])){
                    sb.append("服务上架百度AI审核图片失败，服务图片地址为：" + product.getResources() + ";");
                }
            }else {
                if (!BaiduAuditUtils.ImgCensor(product.getResources())){
                    sb.append("服务上架百度AI审核图片失败，服务图片地址为：" + product.getResources() + ";");
                }
            }
            // 判断AI校验是否成功
            if (sb.length() > 0){
                // 存日志
                addLog(productOnlineAuditLog, product.getId(),0,sb.toString());
                continue;
            }
            // 添加日志
            addLog(productOnlineAuditLog, product.getId(),1,"上架成功！！！");
            // 修改状态
            product.setState(1);
            product.setOnsaletime(date);
            productMapper.update(product);

        }
    }



    /**
     * 下架
     * @param ids
     */
    @Override
    public void offsale(List<Long> ids) {
        productMapper.batchUpdate(ids);
    }


    /**
     * id查询基础信息和详细
     * @param id
     * @return
     */
    @Override
    public Product productAndDetail(Long id) {
        return productMapper.productAndDetail(id);
    }


    /**
     * 存日志
     * @param productOnlineAuditLog
     * @param id
     * @param state
     * @param note
     */
    private void addLog(ProductOnlineAuditLog productOnlineAuditLog, Long id, Integer state,String note) {
        // 添加通过日志
        productOnlineAuditLog.setProductId(id);
        productOnlineAuditLog.setState(state);
        productOnlineAuditLog.setAuditTime(new Date());
        productOnlineAuditLog.setNote(note);
        productOnlineAuditLogMapper.add(productOnlineAuditLog);
    }
}
