package cn.symbio.service.consumer;

import cn.symbio.order.domain.OrderProduct;
import cn.symbio.order.mapper.OrderProductMapper;
import cn.symbio.pet.mapper.PetOnlineAuditLogMapper;
import cn.symbio.service.domain.ProductOnlineAuditLog;
import cn.symbio.service.domain.ProductUntreated;
import cn.symbio.service.mapper.ProductOnlineAuditLogMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@RocketMQMessageListener(consumerGroup = "anran-consumer-group", topic = "anran-topic",
        selectorExpression = "*", messageModel = MessageModel.BROADCASTING)
@Slf4j
public class Consumer implements RocketMQListener<String>{

    @Autowired
    private OrderProductMapper orderProductMapper;

    @Autowired
    private ProductOnlineAuditLogMapper productOnlineAuditLogMapper;

    @Override
    public void onMessage(String message) {
        // System.out.println("消费成功，消费信息为："+message);
        // System.out.println(System.currentTimeMillis());
        // 分割
        String[] split = message.split(":");
        // 获取id
        Long id = Long.valueOf(split[1]);
        // 查询当前
        OrderProduct orderProduct = orderProductMapper.findById(id);
        if (orderProduct.getState() != 1) {
            orderProduct.setState(-1);
        }
        orderProductMapper.update(orderProduct);
        addLog(1L,0,orderProduct.getDigest());
    }

    private void addLog(Long id, Integer state, String note) {
        ProductOnlineAuditLog poal = new ProductOnlineAuditLog();
        poal.setProductId(id);
        poal.setState(state);
        poal.setAuditTime(new Date());
        poal.setNote(note);
        productOnlineAuditLogMapper.add(poal);
    }
}