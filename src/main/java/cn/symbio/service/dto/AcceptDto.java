package cn.symbio.service.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AcceptDto {

    // 服务id
    private Long productId;
    // 接单人
    private Long handler;
    // 备注
    private String note;

}
