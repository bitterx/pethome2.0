package cn.symbio.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductUntreated extends BaseDomain{


    /**
     * 服务名称
     */
    private String name;
    /**
     * 存放fastdfs地址，多个资源使用，分割
     */
    private String resources;
    /**
     * 成本价
     */
    private BigDecimal costprice;
    /**
     * 售价
     */
    private BigDecimal saleprice;
    /**
     * 状态：0接单，1拒单
     */
    private Integer state;
    private Date createtime;
    /**
     * 销售数量
     */
    private Long salecount;
    /**
     * 所属店铺
     */
    private Long shopId;
    private String sname;

    // 谁买的
    private Long userId;
    private String username;

}
