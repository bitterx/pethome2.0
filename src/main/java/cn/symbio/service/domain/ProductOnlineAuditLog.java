package cn.symbio.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductOnlineAuditLog extends BaseDomain{


    /**
     * 消息id
     */
    private Long productId;
    /**
     * 状态 0失败 1成功
     */
    private Integer state;
    /**
     * 审核人 如果为null系统审核
     */
    private Long auditId;
    /**
     * 审核时间
     */
    private Date auditTime;
    /**
     * 备注
     */
    private String note;
}
