package cn.symbio.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDetail extends BaseDomain{

    private Long productId;
    /**
     * 简介
     */
    private String intro;
    /**
     * 预约须知
     */
    private String orderNotice;
}
