package cn.symbio.service.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import cn.symbio.org.domain.Shop;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product extends BaseDomain{


    /**
     * 服务名称
     */
    private String name;
    /**
     * 存放fastdfs地址，多个资源使用，分割
     */
    private String resources;
    /**
     * 成本价
     */
    private BigDecimal costprice;
    /**
     * 售价
     */
    private BigDecimal saleprice;
    /**
     * 下架时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date offsaletime;
    /**
     * 上架时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date onsaletime;
    /**
     * 状态：0下架，1上架
     */
    private Integer state = 0;
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date createtime = new Date();
    /**
     * 销售数量
     */
    private Long salecount;

    // 商品详情
    private ProductDetail detail;
    // 店铺id及名称
    private Long shopId;
    private String sName;
    private Shop shop;

    private Long userId;
    private String username;



}
