package cn.symbio.service.mapper;

import cn.symbio.service.domain.Product;
import cn.symbio.basic.mapper.BaseMapper;
import cn.symbio.service.domain.ProductDetail;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
public interface ProductMapper extends BaseMapper<Product> {

    void batchUpdate(List<Long> ids);

    Product productAndDetail(Long id);
}
