package cn.symbio.service.mapper;

import cn.symbio.service.domain.ProductDetail;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
public interface ProductDetailMapper extends BaseMapper<ProductDetail> {

}
