package cn.symbio.service.mapper;

import cn.symbio.service.domain.ProductOnlineAuditLog;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-11-03
 */
public interface ProductOnlineAuditLogMapper extends BaseMapper<ProductOnlineAuditLog> {

}
