package cn.symbio.service.query;
import cn.symbio.basic.query.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author bitter
 * @since 2022-11-03
 */
@Data
public class ProductQuery extends BaseQuery{

    private Long state;
}