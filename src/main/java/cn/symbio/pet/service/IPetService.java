package cn.symbio.pet.service;

import cn.symbio.pet.domain.Pet;
import cn.symbio.basic.service.IBaseService;
import cn.symbio.pet.domain.PetType;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
public interface IPetService extends IBaseService<Pet> {

    List<PetType> tree();

    void onsale(List<Long> ids);

    void offsale(List<Long> ids);

    void batchDel(List<Long> ids);

    Pet findByPetId(Long id);
}
