package cn.symbio.pet.service;

import cn.symbio.pet.domain.SearchMasterMsgAuditLog;
import cn.symbio.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
public interface ISearchMasterMsgAuditLogService extends IBaseService<SearchMasterMsgAuditLog> {

}
