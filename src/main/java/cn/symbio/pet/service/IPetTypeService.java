package cn.symbio.pet.service;

import cn.symbio.pet.domain.PetType;
import cn.symbio.basic.service.IBaseService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
public interface IPetTypeService extends IBaseService<PetType> {

    // 树
    List<PetType> tree();

}
