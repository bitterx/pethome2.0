package cn.symbio.pet.service;

import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.pet.domain.SearchMasterMsg;
import cn.symbio.basic.service.IBaseService;
import cn.symbio.pet.dto.AcceptDto;
import cn.symbio.pet.query.SearchMasterMsgQuery;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
public interface ISearchMasterMsgService extends IBaseService<SearchMasterMsg> {

    AjaxResult publish(SearchMasterMsg searchMasterMsg);

    PageList<SearchMasterMsg> findSearchMasterMsgByState(SearchMasterMsgQuery searchMasterMsg);

    void reject(Long id);

    void accept(AcceptDto acceptDto);
}
