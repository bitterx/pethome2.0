package cn.symbio.pet.service.impl;

import cn.symbio.pet.domain.SearchMasterMsgAuditLog;
import cn.symbio.pet.service.ISearchMasterMsgAuditLogService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
@Service
public class SearchMasterMsgAuditLogServiceImpl extends BaseServiceImpl<SearchMasterMsgAuditLog> implements ISearchMasterMsgAuditLogService {

}
