package cn.symbio.pet.service.impl;

import cn.symbio.pet.domain.PetOnlineAuditLog;
import cn.symbio.pet.service.IPetOnlineAuditLogService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
@Service
public class PetOnlineAuditLogServiceImpl extends BaseServiceImpl<PetOnlineAuditLog> implements IPetOnlineAuditLogService {

}
