package cn.symbio.pet.service.impl;

import cn.symbio.pet.domain.PetDetail;
import cn.symbio.pet.service.IPetDetailService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
@Service
public class PetDetailServiceImpl extends BaseServiceImpl<PetDetail> implements IPetDetailService {

}
