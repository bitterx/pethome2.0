package cn.symbio.pet.service.impl;

import cn.symbio.pet.domain.PetType;
import cn.symbio.pet.mapper.PetMapper;
import cn.symbio.pet.mapper.PetTypeMapper;
import cn.symbio.pet.service.IPetTypeService;
import cn.symbio.basic.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
@Service
public class PetTypeServiceImpl extends BaseServiceImpl<PetType> implements IPetTypeService {


    @Autowired
    private PetTypeMapper petTypeMapper;


    /**
     * 类型树
     * @return
     */
    @Override
    public List<PetType> tree() {
        List<PetType> all = petTypeMapper.findAll();
        List<PetType> petTypes = new ArrayList<>();
        Map<Long, PetType> collect = all.stream().collect(Collectors.toMap(PetType::getId, petType -> petType));
        for (PetType petType : all) {
            if (null == petType.getPid()) {
                petTypes.add(petType);
            } else {
                PetType petType1 = collect.get(petType.getPid());
                List<PetType> children = petType1.getChildren();
                children.add(petType);
            }
        }
        return petTypes;
    }
}
