package cn.symbio.pet.service;

import cn.symbio.pet.domain.PetDetail;
import cn.symbio.basic.service.IBaseService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
public interface IPetDetailService extends IBaseService<PetDetail> {

}
