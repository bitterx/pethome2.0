package cn.symbio.pet.mapper;

import cn.symbio.pet.domain.SearchMasterMsgAuditLog;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
public interface SearchMasterMsgAuditLogMapper extends BaseMapper<SearchMasterMsgAuditLog> {

}
