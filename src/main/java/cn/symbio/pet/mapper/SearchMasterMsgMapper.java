package cn.symbio.pet.mapper;

import cn.symbio.pet.domain.SearchMasterMsg;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
public interface SearchMasterMsgMapper extends BaseMapper<SearchMasterMsg> {

}
