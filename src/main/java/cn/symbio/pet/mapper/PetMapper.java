package cn.symbio.pet.mapper;

import cn.symbio.pet.domain.Pet;
import cn.symbio.basic.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
public interface PetMapper extends BaseMapper<Pet> {

    // 下架
    void batchUpdate(List<Long> ids);

    void batchOnUpdate(List<Long> ids);

    void batchDel(List<Long> ids);

    Pet findByPetId(Long id);
}
