package cn.symbio.pet.mapper;

import cn.symbio.pet.domain.PetOnlineAuditLog;
import cn.symbio.basic.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
public interface PetOnlineAuditLogMapper extends BaseMapper<PetOnlineAuditLog> {

}
