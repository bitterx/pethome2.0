package cn.symbio.pet.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AcceptDto {

    // 寻主消息id
    private Long msgId;
    // 接单人
    private Long handler;
    // 备注
    private String note;

}
