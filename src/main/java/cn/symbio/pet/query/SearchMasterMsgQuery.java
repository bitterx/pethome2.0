package cn.symbio.pet.query;
import cn.symbio.basic.query.BaseQuery;
import lombok.Data;

/**
 *
 * @author bitter
 * @since 2022-11-01
 */
@Data
public class SearchMasterMsgQuery extends BaseQuery{

    // 状态
    private Long state;
    // 店铺id
    private Long shopId;


}