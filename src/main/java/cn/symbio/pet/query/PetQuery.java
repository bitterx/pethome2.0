package cn.symbio.pet.query;
import cn.symbio.basic.query.BaseQuery;
import lombok.Data;

/**
 *
 * @author bitter
 * @since 2022-10-28
 */
@Data
public class PetQuery extends BaseQuery{

    // 判断是前台用户还是后台用户
    private Integer states;
}