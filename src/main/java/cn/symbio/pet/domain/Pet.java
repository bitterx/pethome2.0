package cn.symbio.pet.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import cn.symbio.org.domain.Shop;
import cn.symbio.user.domain.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Pet extends BaseDomain{

    private String name;
    private BigDecimal saleprice;
    private String costprice;
    private String resources;
    /**
     * 状态：0下架，1上架
     */
    private Integer state = 0;
    /**
     * 类型id
     */
    private Long typeId;
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date offsaletime;
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date onsaletime;
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date createtime = new Date();
    /**
     * 店铺Id 如果被领养店铺id为null
     */
    private Long shopId;
    /**
     * 如果被领养为领养用户id
     */
    private Long userId;
    private Long searchMasterMsgId;

    // 明细
    private PetDetail detail;

    private Shop shop;
    private PetType petType;
    private User user;

}
