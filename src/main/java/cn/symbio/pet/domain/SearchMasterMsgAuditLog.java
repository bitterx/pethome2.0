package cn.symbio.pet.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchMasterMsgAuditLog extends BaseDomain{


    /**
     * 消息id
     */
    private Long msgId;
    /**
     * 状态 0失败 1成功
     */
    private Integer state = 0;
    /**
     * 审核人 如果为null系统审核
     */
    private Long auditId;
    /**
     * 审核时间
     */
    private Date auditTime = new Date();
    /**
     * 备注
     */
    private String note;

}
