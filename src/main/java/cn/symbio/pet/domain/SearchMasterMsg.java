package cn.symbio.pet.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-11-01
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchMasterMsg extends BaseDomain{


    /**
     * 宠物名称
     */
    private String name;
    private Integer age;
    private Boolean gender;
    /**
     * 毛色
     */
    private String coatColor;
    private String resources;
    /**
     * 类型
     */
    private Long petType = 1L;
    private Double price;
    private String address;
    /**
     * 待处理 0 已处理1
     */
    private Integer state = 0;
    private String title;
    private Long userId;
    /**
     * 店铺id 消息分配的店铺
     */
    private Long shopId;

}
