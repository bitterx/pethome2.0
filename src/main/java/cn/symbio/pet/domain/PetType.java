package cn.symbio.pet.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cn.symbio.basic.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
@Data
public class PetType extends BaseDomain{

    private String name;
    private String description;
    /**
     * 父类型id
     */
    private Long pid;

    // 父级名称
    private String pname;


    private List<PetType> children = new ArrayList<>();

}
