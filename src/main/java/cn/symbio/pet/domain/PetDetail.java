package cn.symbio.pet.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
@Data
public class PetDetail extends BaseDomain{


    private Long petId;
    /**
     * 领养须知
     */
    private String adoptNotice;
    /**
     * 简介
     */
    private String intro;


}
