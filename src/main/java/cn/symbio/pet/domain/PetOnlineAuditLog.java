package cn.symbio.pet.domain;

import java.math.BigDecimal;
import java.util.Date;
import cn.symbio.basic.domain.BaseDomain;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

/**
 * <p>
 * 
 * </p>
 *
 * @author bitter
 * @since 2022-10-28
 */
@Data
public class PetOnlineAuditLog extends BaseDomain{


    /**
     * 消息id
     */
    private Long petId;
    /**
     * 状态 0失败 1成功
     */
    private Integer state;
    /**
     * 审核人 如果为null系统审核
     */
    private Long auditId;
    /**
     * 审核时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH-mm-ss")
    private Date auditTime = new Date();
    /**
     * 备注
     */
    private String note;

}
