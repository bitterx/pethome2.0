package cn.symbio.pet.controller;

import cn.symbio.basic.util.LoginContext;
import cn.symbio.log.controller.LogController;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.service.IEmployeeService;
import cn.symbio.pet.dto.AcceptDto;
import cn.symbio.pet.service.ISearchMasterMsgService;
import cn.symbio.pet.domain.SearchMasterMsg;
import cn.symbio.pet.query.SearchMasterMsgQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.user.domain.Logininfo;
import cn.symbio.user.domain.User;
import cn.symbio.user.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/searchMasterMsg")
public class SearchMasterMsgController {
    @Autowired
    public ISearchMasterMsgService searchMasterMsgService;

    @Autowired
    public UserMapper userMapper;

    @Autowired
    private IEmployeeService employeeService;

    /**
     * 保存和修改公用的
     * @param searchMasterMsg  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody SearchMasterMsg searchMasterMsg){
        try {
            if( searchMasterMsg.getId()!=null)
                searchMasterMsgService.update(searchMasterMsg);
            else
                searchMasterMsgService.add(searchMasterMsg);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            searchMasterMsgService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public SearchMasterMsg get(@PathVariable("id")Long id)
    {
        return searchMasterMsgService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<SearchMasterMsg> list(){

        return searchMasterMsgService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<SearchMasterMsg> json(@RequestBody SearchMasterMsgQuery query)
    {
        return searchMasterMsgService.queryByPage(query);
    }

    /**
    * 寻主发布
    */
    @PostMapping("/publish")
    public AjaxResult publish(@RequestBody SearchMasterMsg searchMasterMsg, HttpServletRequest request)
    {
        // 获取logininfo
        Logininfo loginUser = LoginContext.getLoginUser(request);
        // 通过logininfoId查询user
        User user = userMapper.findByLogin(loginUser.getId());
        // 给寻主绑定userid
        searchMasterMsg.setUserId(user.getId());
        return searchMasterMsgService.publish(searchMasterMsg);
    }



    /**
     * 寻主消息已审核并绑定店铺（state=1，shopId=*）
     */
    @PostMapping("/toHandle")
    public PageList<SearchMasterMsg> toHandle(@RequestBody SearchMasterMsgQuery searchMasterMsg, HttpServletRequest request)
    {
        Logininfo loginUser = LoginContext.getLoginUser(request);
        // 获取当前店铺id
        Employee byLoginId = employeeService.findByLoginId(loginUser.getId());
        searchMasterMsg.setShopId(byLoginId.getShopId());
        return searchMasterMsgService.findSearchMasterMsgByState(searchMasterMsg);
    }

    /**
     * 寻主消息待审核（state=0，shopId=null）
     */
    @PostMapping("/toAudit")
    public PageList<SearchMasterMsg> toAudit(@RequestBody SearchMasterMsgQuery searchMasterMsg)
    {
        return searchMasterMsgService.findSearchMasterMsgByState(searchMasterMsg);
    }
    /**
    * 寻主池（state=1，shopId=null）
    */
    @PostMapping("/toSearchMasterPool")
    public PageList<SearchMasterMsg> toSearchMasterPool(@RequestBody SearchMasterMsgQuery searchMasterMsg)
    {
        return searchMasterMsgService.findSearchMasterMsgByState(searchMasterMsg);
    }


    /**
    * 已完成（state=3，shopId=*）
    */
    @PostMapping("/finish")
    public PageList<SearchMasterMsg> finish(@RequestBody SearchMasterMsgQuery searchMasterMsg,HttpServletRequest request)
    {
        Logininfo loginUser = LoginContext.getLoginUser(request);
        // 获取当前店铺id
        Employee byLoginId = employeeService.findByLoginId(loginUser.getId());
        // 区分是店铺管理员还是品台管理员
        if (byLoginId.getShopId() != null ){
            searchMasterMsg.setShopId(byLoginId.getShopId());
        }
        return searchMasterMsgService.findSearchMasterMsgByState(searchMasterMsg);
    }
    /**
    *   拒单
    */
    @PostMapping("/reject/{id}")
    public AjaxResult reject(@PathVariable("id") Long id)
    {
        searchMasterMsgService.reject(id);
        return AjaxResult.me();
    }
    /**
    *   接单
    */
    @PostMapping("/accept")
    public AjaxResult accept(@RequestBody AcceptDto acceptDto)
    {
        searchMasterMsgService.accept(acceptDto);
        return AjaxResult.me();
    }
}
