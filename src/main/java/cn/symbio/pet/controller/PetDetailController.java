package cn.symbio.pet.controller;

import cn.symbio.pet.service.IPetDetailService;
import cn.symbio.pet.domain.PetDetail;
import cn.symbio.pet.query.PetDetailQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/petDetail")
public class PetDetailController {
    @Autowired
    public IPetDetailService petDetailService;


    /**
     * 保存和修改公用的
     * @param petDetail  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody PetDetail petDetail){
        try {
            if( petDetail.getId()!=null)
                petDetailService.update(petDetail);
            else
                petDetailService.add(petDetail);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            petDetailService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public PetDetail get(@PathVariable("id")Long id)
    {
        return petDetailService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<PetDetail> list(){

        return petDetailService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<PetDetail> json(@RequestBody PetDetailQuery query)
    {
        return petDetailService.queryByPage(query);
    }
}
