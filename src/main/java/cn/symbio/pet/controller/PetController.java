package cn.symbio.pet.controller;

import cn.symbio.basic.jwt.JwtUtils;
import cn.symbio.basic.jwt.Payload;
import cn.symbio.basic.jwt.RsaUtils;
import cn.symbio.basic.jwt.UserInFo;
import cn.symbio.basic.util.LoginContext;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.mapper.EmployeeMapper;
import cn.symbio.org.service.IEmployeeService;
import cn.symbio.pet.domain.PetDetail;
import cn.symbio.pet.domain.PetType;
import cn.symbio.pet.service.IPetService;
import cn.symbio.pet.domain.Pet;
import cn.symbio.pet.query.PetQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import cn.symbio.user.domain.Logininfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.PublicKey;
import java.util.List;

@RestController
@RequestMapping("/pet")
public class PetController {
    @Autowired
    public IPetService petService;

    @Autowired
    private IEmployeeService employeeService;

    /**
     * 保存和修改公用的
     * @param pet  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody Pet pet, HttpServletRequest request){
        Logininfo logininfo = LoginContext.getLoginUser(request);
        // 通过logininfo_id查询管理员
        Employee employee = employeeService.findByLoginId(logininfo.getId());
        // 设置店铺id
        pet.setShopId(employee.getShopId());
        try {
            if( pet.getId()!=null)
                petService.update(pet);
            else
                petService.add(pet);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            petService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public Pet get(@PathVariable("id")Long id)
    {
        return petService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<Pet> list(){

        return petService.findAll();
    }

    /**
    * 批量删除
    * @return
    */
    @PostMapping("/dels")
    public AjaxResult dels(@RequestBody List<Long> ids){
        petService.batchDel(ids);
        return AjaxResult.me();
    }



    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<Pet> json(@RequestBody PetQuery query)
    {
        return petService.queryByPage(query);
    }

    /**
    * 宠物类型树
    */
    @GetMapping("/tree")
    public List<PetType> tree()
    {
        return petService.tree();
    }
    /**
    * 上架
    */
    @PostMapping("/onsale")
    public AjaxResult onsale(@RequestBody List<Long> ids)
    {
        petService.onsale(ids);
        return AjaxResult.me();
    }
    /**
    * 下架
    */
    @PostMapping("/offsale")
    public AjaxResult offsale(@RequestBody List<Long> ids)
    {
        petService.offsale(ids);
        return AjaxResult.me();
    }


    /**
     * 通过pet_id获取数据
     * @return
     */
    @GetMapping("/detail/{id}")
    public Pet findByPetId(@PathVariable("id") Long id){
        Pet byPetId = petService.findByPetId(id);
        return byPetId;
    }

}
