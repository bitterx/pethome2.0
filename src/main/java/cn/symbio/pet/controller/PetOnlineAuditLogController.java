package cn.symbio.pet.controller;

import cn.symbio.pet.service.IPetOnlineAuditLogService;
import cn.symbio.pet.domain.PetOnlineAuditLog;
import cn.symbio.pet.query.PetOnlineAuditLogQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/petOnlineAuditLog")
public class PetOnlineAuditLogController {
    @Autowired
    public IPetOnlineAuditLogService petOnlineAuditLogService;


    /**
     * 保存和修改公用的
     * @param petOnlineAuditLog  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody PetOnlineAuditLog petOnlineAuditLog){
        try {
            if( petOnlineAuditLog.getId()!=null)
                petOnlineAuditLogService.update(petOnlineAuditLog);
            else
                petOnlineAuditLogService.add(petOnlineAuditLog);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            petOnlineAuditLogService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public PetOnlineAuditLog get(@PathVariable("id")Long id)
    {
        return petOnlineAuditLogService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<PetOnlineAuditLog> list(){

        return petOnlineAuditLogService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<PetOnlineAuditLog> json(@RequestBody PetOnlineAuditLogQuery query)
    {
        return petOnlineAuditLogService.queryByPage(query);
    }
}
