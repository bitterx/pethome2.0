package cn.symbio.pet.controller;

import cn.symbio.pet.service.ISearchMasterMsgAuditLogService;
import cn.symbio.pet.domain.SearchMasterMsgAuditLog;
import cn.symbio.pet.query.SearchMasterMsgAuditLogQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/searchMasterMsgAuditLog")
public class SearchMasterMsgAuditLogController {
    @Autowired
    public ISearchMasterMsgAuditLogService searchMasterMsgAuditLogService;


    /**
     * 保存和修改公用的
     * @param searchMasterMsgAuditLog  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody SearchMasterMsgAuditLog searchMasterMsgAuditLog){
        try {
            if( searchMasterMsgAuditLog.getId()!=null)
                searchMasterMsgAuditLogService.update(searchMasterMsgAuditLog);
            else
                searchMasterMsgAuditLogService.add(searchMasterMsgAuditLog);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            searchMasterMsgAuditLogService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public SearchMasterMsgAuditLog get(@PathVariable("id")Long id)
    {
        return searchMasterMsgAuditLogService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<SearchMasterMsgAuditLog> list(){

        return searchMasterMsgAuditLogService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<SearchMasterMsgAuditLog> json(@RequestBody SearchMasterMsgAuditLogQuery query)
    {
        return searchMasterMsgAuditLogService.queryByPage(query);
    }
}
