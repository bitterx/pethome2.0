package cn.symbio.pet.controller;

import cn.symbio.pet.service.IPetTypeService;
import cn.symbio.pet.domain.PetType;
import cn.symbio.pet.query.PetTypeQuery;
import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.PageList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/petType")
public class PetTypeController {
    @Autowired
    public IPetTypeService petTypeService;


    /**
     * 保存和修改公用的
     * @param petType  传递的实体
     * @return AjaxResult转换结果
     */
    @PutMapping
    public AjaxResult addOrUpdate(@RequestBody PetType petType){
        try {
            if( petType.getId()!=null)
                petTypeService.update(petType);
            else
                petTypeService.add(petType);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me();
        }
    }
    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @DeleteMapping(value="/{id}")
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            petTypeService.delete(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me();
        }
    }
	
    //获取用户
    @GetMapping("/{id}")
    public PetType get(@PathVariable("id")Long id)
    {
        return petTypeService.findById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @GetMapping()
    public List<PetType> list(){

        return petTypeService.findAll();
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @PostMapping("/list")
    public PageList<PetType> json(@RequestBody PetTypeQuery query)
    {
        return petTypeService.queryByPage(query);
    }
    /**
    * 类型树
    *
    */
    @GetMapping("/tree")
    public List<PetType> tree()
    {
        return petTypeService.tree();
    }
}
