package cn.symbio.basic.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

import java.io.IOException;

@Slf4j
public class SendSmsUtil {


    /**
     *
     * @param phone 手机号
     * @param smsText 内容
     */
    public static void send(String phone,String smsText) {
        try {
            HttpClient client = new HttpClient();
            PostMethod post = new PostMethod("https://utf8api.smschinese.cn/");
            post.addRequestHeader("Content-Type","application/x-www-form-urlencoded;charset=utf-8");//在头文件中设置转码
            NameValuePair[] data ={
                    new NameValuePair("Uid", "bitterx"),
                    new NameValuePair("Key", "5974844B7413238B813A86DFBB710F0E"),
                    new NameValuePair("smsMob",phone),
                    new NameValuePair("smsText",smsText)};
            post.setRequestBody(data);

            client.executeMethod(post);
            Header[] headers = post.getResponseHeaders();
            int statusCode = post.getStatusCode();
            System.out.println("statusCode:"+statusCode); //HTTP状态码
            for(Header h : headers){
                System.out.println(h.toString());
            }
            String result = new String(post.getResponseBodyAsString().getBytes("utf-8"));
            System.out.println(result);  //打印返回消息状态

            post.releaseConnection();
        } catch (IOException e) {
            log.info("发送短信失败,{}",e.getMessage());
        }
    }
}
