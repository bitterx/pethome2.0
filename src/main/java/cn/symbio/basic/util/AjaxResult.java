package cn.symbio.basic.util;

import lombok.Data;

@Data
public class AjaxResult {

    private Boolean success = true;
    private String message = "操作成功";
    //额外的返回参数
    private Object resultObj;

    //调有时出创建对象
    public static  AjaxResult me(){
        return new AjaxResult();
    }
    public static  AjaxResult me(Object resultObj){
        return new AjaxResult().setResultObj(resultObj);
    }

    public AjaxResult fail(String msg){
        return AjaxResult.me().setSuccess(false).setMessage(msg);
    }

    public AjaxResult fail(){
        return AjaxResult.me().setSuccess(false).setMessage("请求失败！");
    }


    public AjaxResult setSuccess(Boolean success) {
        this.success = success;
        return this;
    }

    public AjaxResult setMessage(String message) {
        this.message = message;
        return this;
    }

    public AjaxResult setResultObj(Object resultObj) {
        this.resultObj = resultObj;
        return this;
    }

}
