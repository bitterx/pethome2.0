package cn.symbio.basic.util;

import cn.symbio.basic.jwt.*;
import cn.symbio.user.domain.Logininfo;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.security.PublicKey;

/**
 * 获取登录信息
 */
@Slf4j
public class LoginContext {

    public static Logininfo getLoginUser(HttpServletRequest request) {
        try {
            // Header获取token
            String token = request.getHeader("token");
            // 公钥获取
            PublicKey publicKey = RsaUtils.getPublicKey(JwtRasHolder.INSTANCE.getJwtRsaPubData());
            Payload<UserInFo> payload = JwtUtils.getInfoFromToken(token, publicKey, UserInFo.class);
            UserInFo userInfo = payload.getUserInfo();
            if (null == userInfo) {
                log.error("没有登录哦！！！");
                return null;
            }
            return userInfo.getLogininfo();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

}
