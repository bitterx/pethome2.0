package cn.symbio.basic.service.impl;

import cn.symbio.basic.config.BussinessException;
import cn.symbio.basic.note.PreAuthorize;
import cn.symbio.basic.service.IPermissionScanService;
import cn.symbio.system.domain.Permission;
import cn.symbio.system.mapper.PermissionMapper;
import cn.symbio.basic.util.Classutil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.swing.*;
import java.io.File;
import java.io.FileFilter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Description: 权限初始化类
 * @Author: Mr.She
 * @Version: 1.0
 * @Date: 2022/6/23 22:37
 */
@Service
public class PermissionScanServiceImpl implements IPermissionScanService {


    // 扫描路径包前缀
    private static final String PKG_PREFIX = "cn.symbio.";
    // 扫描路径包后缀
    private static final String PKG_SUFFIX = ".controller";

    @Autowired
    private PermissionMapper permissionMapper;

    /**
     * @Title: scanPermission
     * @Description: 权限初始化业务方法
     * @Author: Mr.She
     * @Version: 1.0
     * @Date: 2022/6/23 22:37
     * @Parameters:
     * @Return void
     */
    @Override
    public void scanPermission() {
        // 1.获取cn.symbio下面所有的模块目录
        String path = this.getClass().getResource("/").getPath() + "/cn/symbio/";
        File file = new File(path);
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory();
            }
        });

        // 2.获取cn.symbio.*.controller里面所有的类
        Set<Class> clazzes = new HashSet<>();
        for (File fileTmp : files) {
            System.out.println(fileTmp.getName());
            clazzes.addAll(Classutil.getClasses(PKG_PREFIX + fileTmp.getName() + PKG_SUFFIX));
        }


        // 3.遍历获取的每个类
        for (Class clazz : clazzes) {
            // 3.1.得到类中的所有方法，如果类中没有方法那么结束本次循环
            Method[] methods = clazz.getMethods();
            // 判断至少有一个方法，没有直接返回
            if (null == methods || methods.length <1) continue;
            // 3.2.遍历类中的所有方法
            for (Method method : methods) {
                // 1.getUri得到该方法的请求地址
                String uri = getUri(clazz,method);
                try {
                // 2.获取到方法上我们的自定义注解，
                    PreAuthorize annotation = method.getAnnotation(PreAuthorize.class);
                    // 判断如果没有此结束本次循环
                    if (null == annotation) continue;
                    // 3.得到我们自定义注解中的name和sn
                    String name = annotation.name();
                    String sn = annotation.sn();
                    // 4.根据sn编码查询权限对象
                    Permission permission = permissionMapper.loadBySn(sn);
                    // 5.如果不存在就添加权限
                    if (null == permission) {
                        Permission permission1 = new Permission();
                        permission1.setName(name);
                        permission1.setSn(sn);
                        permission1.setUrl(uri);
                        permissionMapper.add(permission1);
                    } else {
                        // 6.如果存在就修改权限
                        Permission permission1 = new Permission();
                        permission1.setName(name);
                        permission1.setSn(sn);
                        permission1.setUrl(uri);
                        permissionMapper.update(permission1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new BussinessException("权限录入失败");
                }
            }
        }
    }


    /**
     * @Description: 根据类的字节码和类的方法得到方法的请求地址
     * @Author: July
     * @Date: 2022/7/10 23:37
     * @param clazz: 类字节码对象
     * @param method: 方法对象
     * @return: java.lang.String
     **/
    private String getUri(Class clazz, Method method) {
        String classPath = "";
        Annotation annotation = clazz.getAnnotation(RequestMapping.class);
        if (annotation != null) {
            RequestMapping requestMapping = (RequestMapping) annotation;
            String[] values = requestMapping.value();
            if (values != null && values.length > 0) {
                classPath = values[0];
                if (!"".equals(classPath) && !classPath.startsWith("/"))
                    classPath = "/" + classPath;
            }
        }
        GetMapping getMapping = method.getAnnotation(GetMapping.class);
        String methodPath = "";
        if (getMapping != null) {
            String[] values = getMapping.value();
            if (values != null && values.length > 0) {
                methodPath = values[0];
                if (!"".equals(methodPath) && !methodPath.startsWith("/"))
                    methodPath = "/" + methodPath;
            }
        }
        PostMapping postMapping = method.getAnnotation(PostMapping.class);
        if (postMapping != null) {
            String[] values = postMapping.value();
            if (values != null && values.length > 0) {
                methodPath = values[0];
                if (!"".equals(methodPath) && !methodPath.startsWith("/"))
                    methodPath = "/" + methodPath;
            }
        }
        DeleteMapping deleteMapping = method.getAnnotation(DeleteMapping.class);
        if (deleteMapping != null) {
            String[] values = deleteMapping.value();
            if (values != null && values.length > 0) {
                methodPath = values[0];
                if (!"".equals(methodPath) && !methodPath.startsWith("/"))
                    methodPath = "/" + methodPath;
            }
        }
        PutMapping putMapping = method.getAnnotation(PutMapping.class);
        if (putMapping != null) {
            String[] values = putMapping.value();
            if (values != null && values.length > 0) {
                methodPath = values[0];
                if (!"".equals(methodPath) && !methodPath.startsWith("/"))
                    methodPath = "/" + methodPath;
            }

        }
        PatchMapping patchMapping = method.getAnnotation(PatchMapping.class);
        if (patchMapping != null) {
            String[] values = patchMapping.value();
            if (values != null && values.length > 0) {
                methodPath = values[0];
                if (!"".equals(methodPath) && !methodPath.startsWith("/"))
                    methodPath = "/" + methodPath;
            }
        }
        RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
        if (requestMapping != null) {
            String[] values = requestMapping.value();
            if (values != null && values.length > 0) {
                methodPath = values[0];
                if (!"".equals(methodPath) && !methodPath.startsWith("/"))
                    methodPath = "/" + methodPath;
            }
        }
        return classPath + methodPath;
    }

    private String getPermissionSn(String value) {
        String regex = "\\[(.*?)]";
        Pattern p = Pattern.compile("(?<=\\()[^\\)]+");
        Matcher m = p.matcher(value);
        String permissionSn = null;
        if (m.find()) {
            permissionSn = m.group(0).substring(1, m.group().length() - 1);
        }
        return permissionSn;
    }

}