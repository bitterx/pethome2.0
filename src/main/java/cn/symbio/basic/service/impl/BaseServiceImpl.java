package cn.symbio.basic.service.impl;

import cn.symbio.basic.mapper.BaseMapper;
import cn.symbio.basic.query.BaseQuery;
import cn.symbio.basic.service.IBaseService;
import cn.symbio.basic.util.PageList;
import cn.symbio.org.domain.Department;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

public class BaseServiceImpl<T> implements IBaseService<T> {


    @Autowired
    private BaseMapper<T> mapper;

    @Override
    public void add(T t) {
        mapper.add(t);
    }

    @Override
    public void update(T t) {
        mapper.update(t);
    }

    @Override
    public void delete(Long id) {
        mapper.delete(id);
    }

    @Override
    public List<T> findAll() {
        return mapper.findAll();
    }

    @Override
    public T findById(Long id) {
        return mapper.findById(id);
    }

    @Override
    public PageList<T> queryByPage(BaseQuery query) {
        // 查询条数
        Integer totals = mapper.queryByCount(query);
        // 判断是否有数据
        if (totals == null || totals == 0) {
            return new PageList<>(0,new ArrayList<>());
        }
        // 查询数据
        List<T> departments = mapper.queryByPage(query);
        // 封装返回
        return new PageList<>(totals,departments);
    }

    @Override
    public void patchDel(List<Long> ids) {
        mapper.patchDel(ids);
    }
}
