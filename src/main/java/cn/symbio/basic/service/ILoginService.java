package cn.symbio.basic.service;

import cn.symbio.basic.dto.AccountLoginDto;
import cn.symbio.basic.dto.PhoneLoginDto;
import cn.symbio.basic.dto.WechatLoginDto;
import cn.symbio.basic.util.AjaxResult;

import java.util.Map;

public interface ILoginService {
    // 账号密码登录
    Map<String, Object> accountLogin(AccountLoginDto dto);

    // 手机号登录
    Map<String, Object> phoneLogin(PhoneLoginDto dto);

    // 微信扫码登录
    AjaxResult wechatLogin(WechatLoginDto dto);

    // 微信绑定
    Map<String, Object> wechatBinder(WechatLoginDto dto);
}
