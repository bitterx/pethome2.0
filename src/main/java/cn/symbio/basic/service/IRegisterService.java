package cn.symbio.basic.service;

import cn.symbio.basic.dto.EmailRegisterDto;
import cn.symbio.basic.dto.PhoneRegisterDto;

public interface IRegisterService {
    void phoneRegister(PhoneRegisterDto dto);

    void emailRegister(EmailRegisterDto dto);
}
