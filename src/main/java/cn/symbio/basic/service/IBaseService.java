package cn.symbio.basic.service;

import cn.symbio.basic.query.BaseQuery;
import cn.symbio.basic.util.PageList;

import java.util.List;

public interface IBaseService<T> {

    // 添加一条数据
    void add(T t);

    // 修改一条数据
    void update(T t);

    // 删除一条数据
    void delete(Long id);

    // 查询全部
    List<T> findAll();

    // 查询一条
    T findById(Long id);

    // 分页查询
    PageList<T> queryByPage(BaseQuery tQuery);

    // 批量删除
    void patchDel(List<Long> ids);

}
