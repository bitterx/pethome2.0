package cn.symbio.basic.service;

import cn.symbio.basic.dto.EmailDto;
import cn.symbio.basic.dto.PhoneLoginDto;
import cn.symbio.basic.dto.RegisterDto;

public interface IVerifyCodeService {

    String verifyCode(String key);

    void smsCode(RegisterDto registerDto);



    void emailCode(EmailDto emailDto);
}
