package cn.symbio.basic.jwt;

import lombok.Data;

import java.util.Date;

@Data
public class Payload<T> {

    private String id;  // jwt的id(token) 唯一标识
    private T userInfo;  // 存储用户信息、权限信息、菜单信息
    private Date expiration;  // JWT串过期时间
}
