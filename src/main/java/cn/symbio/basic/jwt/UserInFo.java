package cn.symbio.basic.jwt;

import cn.symbio.system.domain.Menu;
import cn.symbio.user.domain.Logininfo;
import lombok.Data;

import java.util.List;

@Data
public class UserInFo {

    private Logininfo logininfo;
    private List<String> permissions;
    private List<Menu> menus;

}
