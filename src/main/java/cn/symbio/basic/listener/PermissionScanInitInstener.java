package cn.symbio.basic.listener;


import cn.symbio.basic.service.IPermissionScanService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

// ServletContextListener,spring的监听
// 表示为自定义监听器
@WebListener
@Slf4j
public class PermissionScanInitInstener implements ServletContextListener {


    @Autowired
    private IPermissionScanService permissionScanService;


    /**
     *   Spring容器初始化之后被调用
     * @param servletContextEvent
     */
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("contextInitialized start...");
                // 执行permission数据添加方法
                permissionScanService.scanPermission();
                log.info("contextInitialized stop...");
            }
        }).start();

    }

    /**
     * 程序停止时被调用
     * @param servletContextEvent
     */
    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.info("over.......");
    }
}
