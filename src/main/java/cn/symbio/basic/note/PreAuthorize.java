package cn.symbio.basic.note;

import java.lang.annotation.*;

@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PreAuthorize {

    // 注解的第一个值：permission的sn字段值
    String sn() default "";

    // 注解的第二个值：permission的name字段值
    String name() default "";
}
