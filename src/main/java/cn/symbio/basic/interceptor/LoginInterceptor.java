package cn.symbio.basic.interceptor;

import cn.symbio.basic.jwt.JwtUtils;
import cn.symbio.basic.jwt.Payload;
import cn.symbio.basic.jwt.RsaUtils;
import cn.symbio.basic.jwt.UserInFo;
import cn.symbio.basic.note.PreAuthorize;
import cn.symbio.system.mapper.MenuMapper;
import cn.symbio.system.mapper.PermissionMapper;
import cn.symbio.user.domain.Logininfo;
import org.omg.PortableInterceptor.Interceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;
import java.util.concurrent.TimeUnit;

// 交由spring管理
@Component
public class LoginInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private PermissionMapper permissionMapper;


    @Value("jwt.rsa.pub")
    private String publicYmlKey;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 通过请求获取token
        String token = request.getHeader("token");
        // 判断是否存在
        if (null != token) {
            // 存在说明登录了,再去判断用户是否存在
            // Logininfo logininfo = (Logininfo) redisTemplate.opsForValue().get(token);
            // 获取
            // TODO 好像使用value取不到yml配置的
            PublicKey publicKey = RsaUtils.getPublicKey(JwtUtils.class.getClassLoader().getResource("jwt/pethome_auth_rsa.pub").getFile());
            Payload<UserInFo> payload = JwtUtils.getInfoFromToken(token, publicKey, UserInFo.class);
            if (null != payload) {
                // 通过payload获取userInfo
                UserInFo userInfo = payload.getUserInfo();
                Logininfo logininfo = userInfo.getLogininfo();
                // 判断当前登录人是后端管理员
                if (1 != logininfo.getType()) {
                    // handler中获取请求方法
                    HandlerMethod handlerMethod = (HandlerMethod) handler;
                    // 获取请求方法上的注解
                    PreAuthorize methodAnnotation = handlerMethod.getMethodAnnotation(PreAuthorize.class);
                    // 首先判断注解不为空
                    if (null != methodAnnotation) {
                        // 再从注解中获取sn
                        String sn = methodAnnotation.sn();
                        // 去数据库查询当前人的权限
                        List<String> list = userInfo.getPermissions();
                        // 使用集合方法判断当前sn是否在当前人权限集合中
                        if (!list.contains(sn)){
                            // 不存在就拦截
                            // 3.1.设置编码字符集
                            response.setCharacterEncoding("UTF-8");
                            // 3.2.设置响应参数类型为JSON
                            response.setContentType("application/json;charset=UTF-8");
                            // 3.3.获取到打印流,并设置响应数据
                            PrintWriter writer = response.getWriter();
                            writer.write("{\"success\":false,\"resultObj\":\"noPermission\"}");
                            // 3.4.刷新、关流
                            writer.flush();
                            writer.close();
                            return false;
                        }
                    }
                }
                // 刷新过期时间
//                redisTemplate.expire(token,30, TimeUnit.MINUTES);
                return true;
            }
        }
        // 3.如果能走到这里，说明token为空或者是Redis中用户信息为空，那么以未登录进行处理
        // 3.1.设置编码字符集
        response.setCharacterEncoding("UTF-8");
        // 3.2.设置响应参数类型为JSON
        response.setContentType("application/json;charset=UTF-8");
        // 3.3.获取到打印流,并设置响应数据
        PrintWriter writer = response.getWriter();
        writer.write("{\"success\":false,\"resultObj\":\"noLogin\"}");
        // 3.4.刷新、关流
        writer.flush();
        writer.close();
        return false;
    }
}
