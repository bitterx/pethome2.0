package cn.symbio.basic.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class PhoneLoginDto {

    @NotBlank(message = "手机号不能为空")
    private String phone;
    private String phoneCodeValue;
    @NotNull(message = "类型不能为空")
    private Integer type;

}
