package cn.symbio.basic.dto;


import lombok.Data;


@Data
public class RegisterDto {
    // 手机号
    private String phone;
    // 图形验证码
    private String imageCodeValue;
    // 存到redis中图形验证码的key
    private String imageCodeKey;
    // 类型
    private String types;
}
