package cn.symbio.basic.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class EmailDto {

    // 邮箱号
    @NotBlank(message = "邮箱号不能为空")
    private String email;
    // 图形验证码
    @NotBlank(message = "图形验证码不能为空")
    private String imageCodeValue;
    // 存到redis中图形验证码的key
    @NotBlank(message = "Redis中key不能为空")
    private String imageCodeKey;
}
