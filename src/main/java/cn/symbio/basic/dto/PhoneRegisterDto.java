package cn.symbio.basic.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class PhoneRegisterDto {

    // 手机号
    @NotBlank(message = "手机号不能为空")
    private String phone;
    // 短信验证码
    @NotBlank(message = "短信验证码不能为空")
    private String smsCodeValue;
    // 密码
    @NotBlank(message = "密码不能为空")
    private String password;
    // 再次密码
    @NotBlank(message = "再次密码不能为空")
    private String passwordRepeat;

}
