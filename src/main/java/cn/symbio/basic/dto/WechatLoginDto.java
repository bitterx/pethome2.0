package cn.symbio.basic.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class WechatLoginDto {


    private String code;
    private String phone;
    private String verifyCode;
    private String accessToken;
    private String openId;
    private Integer type;

}
