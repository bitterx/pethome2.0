package cn.symbio.basic.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class EmailRegisterDto {

    // 邮箱号
    @NotBlank(message = "邮箱号不能为空")
    private String email;
    // 短信验证码
    @NotBlank(message = "邮箱验证码不能为空")
    private String emailCodeValue;
    // 密码
    @NotBlank(message = "密码不能为空")
    private String emailPassword;
    // 再次密码
    @NotBlank(message = "再次密码不能为空")
    private String emailPasswordRepeat;

}
