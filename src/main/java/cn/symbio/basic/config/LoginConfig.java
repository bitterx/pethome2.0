package cn.symbio.basic.config;

import cn.symbio.basic.interceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

// springboot配置类
@Configuration
public class LoginConfig implements WebMvcConfigurer {


    @Autowired
    private LoginInterceptor loginInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginInterceptor)
                // 拦截所以请求
                .addPathPatterns("/**")
                // 放行的请求
                // 登录
                .excludePathPatterns("/login/**")
                // 注册
                .excludePathPatterns("/register/**")
                // fastDFS
                .excludePathPatterns("/fastDfs/**")
                // 验证码
                .excludePathPatterns("/verifyCode/**")
                // 店铺入住
                .excludePathPatterns("/shop/settlement/**");
    }
}
