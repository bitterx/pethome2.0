package cn.symbio.basic.config;

/**
 * 自定义异常
 */
public class BussinessException extends RuntimeException{

    public BussinessException(String message) {
        super(message);
    }
}
