package cn.symbio.basic.query;

import lombok.Data;

@Data
public class BaseQuery {
    // 当前页1
    private Integer currentPage;
    // 每页条数
    private Integer pageSize;
    // 高级查询关键字
    private String keyword;


    // 起始条数
    public Integer getStart(){
        return (currentPage -1) * pageSize;
    }
}
