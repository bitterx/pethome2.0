package cn.symbio.basic.mapper;


import cn.symbio.basic.query.BaseQuery;

import java.util.List;

public interface BaseMapper<T> {


    // 添加一条数据
    void add(T t);

    // 修改一条数据
    void update(T t);

    // 删除一条数据
    void delete(Long id);

    // 查询所有数据
    // @Select("select * from t_t")
    List<T> findAll();

    // 查询一条数据
    T findById(Long id);

    // 查询条数
    Integer queryByCount(BaseQuery tQuery);

    // 查询当前页数据
    List<T> queryByPage(BaseQuery tQuery);

    // 批量删除
    void patchDel(List<Long> ids);

}
