package cn.symbio.basic.constant;

/**
 * 常量
 */
public class BaseConstant {

    /**
     * 注册
     */
    public class VerfityCodeCons {
        // 手机常量
        public static final String REGISTER_SMSCODE_PHONE = "register:smscode:%s";
        // 邮箱常量
        public static final String REGISTER_SMSCODE_EMAIL = "register:smscode:%s";
    }

    /**
     * 微信扫码常量
     */
    public class WechatVerfityCodeCons {
        // APPID
        // - APPID：wxd853562a0548a7d0
        public static final String WECHAT_LOGIN_APPID = "wxd853562a0548a7d0";

        // - SECRET：4a5d5615f93f24bdba2ba8534642dbb6
        public static final String WECHAT_LOGIN_SECRET = "4a5d5615f93f24bdba2ba8534642dbb6";
        // 微信code获取token路径
        public static final String WECHAT_LOGIN_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";
        // 微信登陆请求获取用户信息
        public static final String WECHAT_LOGIN_USER_INFO = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s";

    }
}
