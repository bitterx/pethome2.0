package cn.symbio.basic.controller;

import cn.symbio.basic.util.AjaxResult;
import cn.symbio.basic.util.FastdfsUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/fastDfs")
public class FastDfsController {



    @PostMapping
    public AjaxResult upload(@RequestPart("file") MultipartFile file) throws IOException {
        // 获取文件全限定名
        String originalFilename = file.getOriginalFilename();
        // 获取扩展名
        String name = FilenameUtils.getName(originalFilename);
        // 上传到fastDfs
        String upload = FastdfsUtil.upload(file.getBytes(), name);
        // 返回给前端
        return AjaxResult.me(upload);
    }


    @DeleteMapping
    public AjaxResult delete(@RequestParam(value = "filePath") String filePath) {
// /group1/M00/02/07/CgAIC2NKYxKAbv73AAD2qq2Zn5g619.jpg
        // 因为删除需要组名+图片路径，所以我们需要把前端传递进来的图片路径进行分割
        // /group1/M00/00/05/oYYBAGKCO-6AdKyoAAEOykzR2Hc97_350x350.jpeg
        // 1.得到组名和路径名称
        // 1.1.得到组名
        String pathTmp = filePath.substring(1); // goup1/xxxxx/yyyy
        String groupName =  pathTmp.substring(0, pathTmp.indexOf("/")); //goup1
        // 1.2.得到图片路径
        String remotePath = pathTmp.substring(pathTmp.indexOf("/")+1);// /xxxxx/yyyy
        // 2.调用工具类方法进行删除，传递组名和图片路径名称
        FastdfsUtil.delete(groupName, remotePath);
        return AjaxResult.me();
    }


}
