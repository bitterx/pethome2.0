package cn.symbio.basic.controller;

import cn.symbio.basic.dto.EmailDto;
import cn.symbio.basic.dto.PhoneLoginDto;
import cn.symbio.basic.dto.RegisterDto;
import cn.symbio.basic.service.IVerifyCodeService;
import cn.symbio.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/verifyCode")
public class VerifyCodeController {

    @Autowired
    private IVerifyCodeService verifyCodeService;


    /**
     * 图型验证码
     * @param key
     * @return
     */
    @GetMapping("/image/{key}")
    public AjaxResult verifyCode(@PathVariable("key") String key){
        // base64代码
        String s = verifyCodeService.verifyCode(key);
        return AjaxResult.me().setResultObj(s);
    }




    /**
     * 手机注册验证码
     * @param
     * @return
     */
    @PostMapping("/smsCode")
    public AjaxResult smsCode(@RequestBody @Valid RegisterDto registerDto){
        verifyCodeService.smsCode(registerDto);
        return AjaxResult.me();
    }
    /**
     * 手机登录验证码
     * @param
     * @return
     */
    @PostMapping("/phoneCode")
    public AjaxResult phoneCode(@RequestBody @Valid RegisterDto registerDto){
        verifyCodeService.smsCode(registerDto);
        return AjaxResult.me();
    }
    /**
     * 邮箱注册验证码
     * @param
     * @return
     */
    @PostMapping("/emailCode")
    public AjaxResult emailCode(@RequestBody @Valid EmailDto emailDto){
        verifyCodeService.emailCode(emailDto);
        return AjaxResult.me();
    }



}
