package cn.symbio.basic.controller;

import cn.symbio.basic.dto.EmailRegisterDto;
import cn.symbio.basic.dto.PhoneRegisterDto;
import cn.symbio.basic.service.IRegisterService;
import cn.symbio.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private IRegisterService registerService;

    /**
     * 手机注册
     * @param dto
     * @return
     */
    @PostMapping("/phone")
    public AjaxResult phoneRegister(@RequestBody @Valid PhoneRegisterDto dto){
        registerService.phoneRegister(dto);
        return AjaxResult.me();
    }
    /**
     * 邮箱注册
     * @param dto
     * @return
     */
    @PostMapping("/email")
    public AjaxResult emailRegister(@RequestBody @Valid EmailRegisterDto dto){
        // 邮箱注册
        registerService.emailRegister(dto);
        return AjaxResult.me();
    }



}
