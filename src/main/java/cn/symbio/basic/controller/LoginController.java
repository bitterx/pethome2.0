package cn.symbio.basic.controller;

import cn.symbio.basic.dto.AccountLoginDto;
import cn.symbio.basic.dto.PhoneLoginDto;
import cn.symbio.basic.dto.WechatLoginDto;
import cn.symbio.basic.service.ILoginService;
import cn.symbio.basic.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/login")
public class LoginController {


    @Autowired
    private ILoginService loginService;

    /**
     * 账号密码登录
     * @param dto
     * @return
     */
    @PostMapping("/account")
    public AjaxResult accountLogin(@RequestBody @Valid AccountLoginDto dto){
        Map<String,Object> map = loginService.accountLogin(dto);
        return AjaxResult.me().setResultObj(map);
    }


    /**
     * 手机短信登录
     * @param dto
     * @return
     */
    @PostMapping("/phone")
    public AjaxResult phoneLogin(@RequestBody @Valid PhoneLoginDto dto){
        Map<String,Object> map = loginService.phoneLogin(dto);
        return AjaxResult.me().setResultObj(map);
    }

    /**
     * 微信登陆
     * @param dto
     * @return
     */
    @PostMapping("/wechat")
    public AjaxResult wechatLogin(@RequestBody WechatLoginDto dto){
        return loginService.wechatLogin(dto);
    }

    /**
     * 微信绑定
     * @param dto
     * @return
     */
    @PostMapping("/wechat/binder")
    public AjaxResult wechatBinder(@RequestBody WechatLoginDto dto){
        Map<String,Object> map = loginService.wechatBinder(dto);
        return AjaxResult.me().setResultObj(map);
    }


}
