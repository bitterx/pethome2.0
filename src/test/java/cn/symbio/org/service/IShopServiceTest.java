package cn.symbio.org.service;


import cn.symbio.PethomeApp;
import cn.symbio.org.domain.Shop;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PethomeApp.class)
public class IShopServiceTest {


    @Autowired
    private IShopService shopService;

    @Test
    public void test() {
        List<Shop> all = shopService.findAll();
        all.forEach(System.out::println);
    }
}