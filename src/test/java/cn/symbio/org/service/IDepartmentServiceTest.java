package cn.symbio.org.service;

import cn.symbio.PethomeApp;
import cn.symbio.org.domain.Department;
import org.apache.ibatis.javassist.ClassPath;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PethomeApp.class)
public class IDepartmentServiceTest {

    @Autowired
    private IDepartmentService departmentService;

    @Test
    public void findAll() {
        departmentService.findAll().forEach(System.out::println);
    }
    @Test
    public void findById() {
        Department byId = departmentService.findById(2L);
        System.out.println(byId);
    }
}