package cn.symbio.org.service;

import cn.symbio.PethomeApp;
import cn.symbio.org.domain.Employee;
import cn.symbio.org.mapper.EmployeeMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.PipedReader;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PethomeApp.class)
public class IEmployeeServiceTest {

    @Autowired
    private EmployeeMapper employeeMapper;

    @Test
    public void test() {
        Employee byId = employeeMapper.findById(378L);
        System.out.println(byId);
    }
    @Test
    public void test2() {
        List<Employee> all = employeeMapper.findAll();
        all.forEach(System.out::println);
    }

}