package cn.symbio.org.mytest;

import cn.symbio.PethomeApp;
import cn.symbio.org.domain.Department;
import cn.symbio.org.service.IDepartmentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PethomeApp.class)
public class RedisTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private IDepartmentService departmentService;

    @Test
    public void test1() {
        ValueOperations vo = redisTemplate.opsForValue();
        vo.set("name","zhangsan");
        Object name = vo.get("name");
        System.out.println(name);
    }
    @Test
    public void test2() {
        // 存对象需要序列化
        List<Department> all = departmentService.findAll();
        // 存对象需要序列化
        Department byId = departmentService.findById(1L);
        ValueOperations vo = redisTemplate.opsForValue();
        vo.set("all",all);
        Object dept = vo.get("all");
        System.out.println(dept);
    }

}
