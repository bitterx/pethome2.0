package cn.symbio.org.mytest;


import cn.symbio.PethomeApp;
import cn.symbio.basic.config.BussinessException;
import cn.symbio.basic.util.EmailUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = PethomeApp.class)
public class EmailTest {


    // 注入邮件对象
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private EmailUtils emailUtils;

    @Test
    public void test() {
        SimpleMailMessage si = new SimpleMailMessage();
        si.setFrom("3489571551@qq.com");
        si.setTo("834291384@qq.com");
        si.setSubject("嗨害嗨");
        si.setText("嗨害嗨");
        javaMailSender.send(si);
    }
    @Test
    public void test2() throws MessagingException {
        // 1.创建复杂邮件对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        // 2.发送复杂邮件的工具类
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,true,"utf-8");
        helper.setFrom("978395359@qq.com");
        helper.setSubject("测试复杂邮件");
        // 3.设置文本内容，第二个参数 true 代表是否是一个html内容
        helper.setText("<h1>男德经</h1>"+
                "<img src='http://dfs.java.itsource.cn/group1/M00/00/05/oYYBAGKDLDuADv2ZAABvwky9b5015.jpeg\"' />",true);
        // 4.添加附件
        helper.addAttachment("好东西.jpg",new File("C:\\Users\\colin\\Desktop\\1.jpg"));
        // 5.设置收件人
        helper.setTo("978395359@qq.com");
        // 6.发送邮件
        javaMailSender.send(mimeMessage);
    }


    @Test
    public void test3() {
        try {
            EmailUtils.sendEmail("3489571551@qq.com",
                    "被告",
                    "成*市公*局：天府新*分局\n" +
                            "\n" +
                            "被举报人：李京城\n" +
                            "\n" +
                            "报案请求事项：\n" +
                            "\n" +
                            "请求依法追被举报人涉嫌集资诈骗罪的刑事责任，以挽回报案人的经济损失。\n" +
                            "\n" +
                            "案情事实与经过：\n" +
                            "\n" +
                            "被举报人在2022年10月1日，2022年10月3日，2022年10月7日，先后三次以资金周转名义向报案人借款，出具借款担保合同，承诺借款利率执行年利率20%，借款期限：三个月，按月付息，借款到期，利随本清，并以银行转账和其他形式向报案人非法募集资金人民币：肆佰陆拾万元，到期后不能还款，给报案人造成了巨大的经济损失。事件发生后，报案人寻找被举报人李京城未果，电话停机，另一举报人老毕否认与此事件有关，那么被举报人李京城及其投资公司就成了本事件的主要责任单位及责任人。\n" +
                            "\n" +
                            "被举报人违法国家金融管理法律规定，虚假借款人以借款为名，骗取钱财进行诈骗，仅从报案人处就募集到资金人民币肆佰陆拾万元。被举报人虚构事实，以高息为诱，设置骗局，虚拟借款人，公司做担保人，将诈骗款收取，使用诈骗方式集资，数额巨大的行为触犯了《xxx刑法》第一百九十二条的规定，涉嫌集资诈骗罪。\n" +
                            "\n" +
                            "综上所述，被举报人涉嫌犯罪的行为触犯我国刑法，为使报案人合法权益得到保障，报案人决定向公*机关报案。请求公*机关依法立案，追回受害人蒙受的经济损失，并追究被举报人的刑事责任。\n" +
                            "\n" +
                            "报案人：贺孝泰\n" +
                            "\n" +
                            "2022年 10月 16日",
                    "834291384@qq.com");
        } catch (Exception e) {
            throw new BussinessException("邮件发送失败！！");
        }
    }
    @Test
    public void test4() {
        // 1.创建复杂邮件对象
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        // 2.发送复杂邮件的工具类
        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(mimeMessage,true,"utf-8");
            helper.setFrom("3489571551@qq.com");
            helper.setSubject("店铺审核结果");
            // 3.设置文本内容，第二个参数 true 代表是否是一个html内容
            helper.setText("<h1>恭喜你审核通过，点击下方连接即可激活</h1><br/>"+
                    "http://localhost/shop/uptState/"+1,true);
            // 5.设置收件人
            helper.setTo("834291384@qq.com");
            // 6.发送邮件
            javaMailSender.send(mimeMessage);
            System.out.println("发送邮件成功！！！");
        } catch (MessagingException e) {
            throw new BussinessException("邮件发送失败");
        }
    }
    @Test
    public void test5() {
        try {
            EmailUtils.sendEmail("3489571551@qq.com",
                    "店铺审核结果",
                    "<h1>很遗憾你审核未通过，点击下方连接了解详细</h1><br/>"+"http://localhost:8081/#/registerAgain?shopId="+1,"3489571551@qq.com");
        } catch (Exception e) {
            throw new BussinessException("邮件发送失败！！");
        }
    }


}
